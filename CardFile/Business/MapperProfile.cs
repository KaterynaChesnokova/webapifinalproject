﻿using AutoMapper;
using Business.Models;
using Data.DataEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    /// <summary>
    /// automapper configuration
    /// </summary>
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<Author, GetAuthorModel>()
                .ForMember(x => x.TextsIds, f => f.MapFrom(author => author.AuthorTextMaterials.Select(z => z.TextMaterialId)))
                .ReverseMap();

            CreateMap<GetAuthorModel, Author>()
                .ForMember(x => x.AuthorTextMaterials, f => f.MapFrom(z => z.TextsIds.Select(
                         n => new AuthorTextMaterial { AuthorId = z.Id, TextMaterialId = n })));

            CreateMap<AddAuthorModel, Author>()
                .ForMember(x => x.AuthorTextMaterials, f => f.Ignore())
                .ForMember(x => x.Id, f => f.Ignore());


            CreateMap<Genre, GetGenreModel>()
                .ForMember(x => x.TextsIds, f => f.MapFrom(genre => genre.GenreTextMaterials.Select(z => z.TextMaterialId)));

            CreateMap<GetGenreModel, Genre>()
                .ForMember(x => x.GenreTextMaterials, f => f.MapFrom(genre => genre.TextsIds.Select(
                    n => new GenreTextMaterial { GenreId = genre.Id, TextMaterialId = n })));

            CreateMap<AddGenreModel, Genre>()
               .ForMember(x => x.GenreTextMaterials, f => f.Ignore())
               .ForMember(x => x.Id, f => f.Ignore());


            CreateMap<User, UserModel>()
                .ForMember(x => x.TextsIds, f => f.MapFrom(user => user.Texts.Select(z => z.Id)));

            CreateMap<UserModel, User>()
                .ForMember(x => x.Texts, f => f.MapFrom(z => z.TextsIds.Select(
                         n => new TextMaterial { Id = n })));


            CreateMap<TextMaterial, AddTextMaterialModel>()
                .ForMember(x => x.AuthorsIds, f => f.MapFrom(text => text.AuthorTextMaterials.Select(z => z.AuthorId)))
                .ForMember(x => x.GenresIds, f => f.MapFrom(text => text.GenreTextMaterials.Select(z => z.GenreId)));

            CreateMap<AddTextMaterialModel, TextMaterial>()
                .ForMember(x => x.GenreTextMaterials, f => f.MapFrom(text => text.GenresIds.Select(
                     n => new GenreTextMaterial { GenreId = n })))
                .ForMember(x => x.AuthorTextMaterials, f => f.MapFrom(text => text.AuthorsIds.Select(
                     n => new AuthorTextMaterial { AuthorId = n })));


            CreateMap<TextMaterial, UpdateTextMaterialModel>()
                .ForMember(x => x.AuthorsIds, f => f.MapFrom(text => text.AuthorTextMaterials.Select(z => z.AuthorId)))
                .ForMember(x => x.GenresIds, f => f.MapFrom(text => text.GenreTextMaterials.Select(z => z.GenreId)));

            CreateMap<UpdateTextMaterialModel, TextMaterial>()
                .ForMember(x => x.GenreTextMaterials, f => f.MapFrom(text => text.GenresIds.Select(
                     n => new GenreTextMaterial { GenreId = n, TextMaterialId = text.Id })))
                .ForMember(x => x.AuthorTextMaterials, f => f.MapFrom(text => text.AuthorsIds.Select(
                     n => new AuthorTextMaterial { AuthorId = n, TextMaterialId = text.Id })));


            CreateMap<TextMaterial, GetTextMaterialModel>()
                .ForMember(x => x.AuthorsIds, f => f.MapFrom(text => text.AuthorTextMaterials.Select(z => z.AuthorId)))
                .ForMember(x => x.GenresIds, f => f.MapFrom(text => text.GenreTextMaterials.Select(z => z.GenreId)));
        }
    }
}
