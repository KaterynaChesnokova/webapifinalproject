﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Business.Exceptions
{
    /// <summary>
    /// exception that is thrown when action cannot be made as
    /// needed data was not found
    /// </summary>
    [Serializable]
    public class CardFileNotFoundException : Exception
    {
        public CardFileNotFoundException() { }

        public CardFileNotFoundException(string message) : base(message) { }

        public CardFileNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected CardFileNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
