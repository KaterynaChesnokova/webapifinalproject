﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Business.Exceptions
{
    /// <summary>
    /// exception that is thrown when invalid data is passed to action method
    /// </summary>
    [Serializable]
    public class CardFileInvalidDataException : Exception
    {
        public CardFileInvalidDataException() { }

        public CardFileInvalidDataException(string message) : base($"Invalid data: {message}") { }

        public CardFileInvalidDataException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected CardFileInvalidDataException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
