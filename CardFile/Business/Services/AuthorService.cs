﻿using AutoMapper;
using Business.Exceptions;
using Business.Models;
using Business.ServicesInterfaces;
using Data.DataEntities;
using Data.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Services
{
    /// <summary>
    /// Class contains business logic for actions with authors
    /// </summary>
    public class AuthorService : IAuthorService
    {
        readonly IUnitOfWork uow;
        readonly IMapper mapper;

        public AuthorService(IUnitOfWork _uow, IMapper profile)
        {
            uow = _uow;
            mapper = profile;
        }

        /// <summary>
        /// adds new author asynchronously
        /// </summary>
        /// <param name="model">author to add</param>
        /// <returns>Task</returns>
        public async Task AddAsync(AddAuthorModel model)
        {
            try
            {
                await uow.AuthorRepository.AddAsync(mapper.Map<Author>(model));
                await uow.SaveAsync();
            }
            catch
            {
                throw new CardFileInvalidDataException("This author is already added.");
            }
        }

        /// <summary>
        /// deletes author by id asynchronously
        /// </summary>
        /// <param name="modelId">id of author that shoud be deleted</param>
        /// <returns>Task</returns>
        public async Task DeleteByIdAsync(int modelId)
        {
            var author = await uow.AuthorRepository.GetByIdWithDetailsAsync(modelId);

            if (author == null)
                throw new CardFileNotFoundException($"Author with id {modelId} was not found.");

            if (author.AuthorTextMaterials.Count != 0)
                throw new CardFileInvalidDataException("author cannot be deleted because there are already text materials related to it.");

            uow.AuthorRepository.Delete(author);
            await uow.SaveAsync();
        }

        /// <summary>
        /// returns collection of all authors with details
        /// </summary>
        /// <returns>Collection of authors</returns>
        public IEnumerable<GetAuthorModel> GetAll()
        {
            return mapper.Map<IEnumerable<GetAuthorModel>>(uow.AuthorRepository.GetAllWithDetails());
        }

        /// <summary>
        /// returns author by id asynchronously
        /// </summary>
        /// <param name="id">id of author that shoud be returned</param>
        public async Task<GetAuthorModel> GetByIdAsync(int id)
        {
            return mapper.Map<GetAuthorModel>(await uow.AuthorRepository.GetByIdAsync(id));
        }

        /// <summary>
        /// returns author by id with details asynchronously
        /// </summary>
        /// <param name="id">id of author that shoud be returned</param>
        public async Task<GetAuthorModel> GetByIdWithDetailsAsync(int id)
        {
            var obj = await uow.AuthorRepository.GetByIdWithDetailsAsync(id);
            return mapper.Map<GetAuthorModel>(obj);
        }
    }
}
