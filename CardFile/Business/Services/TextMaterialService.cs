﻿using AutoMapper;
using Business.Exceptions;
using Business.Models;
using Business.ServicesInterfaces;
using Data.DataEntities;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Services
{
    /// <summary>
    /// Class contains business logic for actions with text material
    /// </summary>
    public class TextMaterialService : ITextMaterialService
    {
        readonly IUnitOfWork uow;
        readonly IMapper mapper;

        public TextMaterialService(IUnitOfWork _uow, IMapper profile)
        {
            uow = _uow;
            mapper = profile;
        }

        /// <summary>
        /// validates input model
        /// </summary>
        /// <param name="model">object that need to be checked</param>
        public void CheckAddText(AddTextMaterialModel model)
        {
            if (model.PublisherId == string.Empty)
                throw new CardFileInvalidDataException("publisher is undefined.");
            if (uow.UserRepository.UserFindByIdAsync(model.PublisherId).Result == null)
                throw new CardFileInvalidDataException("invalid publisher id.");
            if (model.PublishDate > DateTime.Now)
                throw new CardFileInvalidDataException("text material publish date is invalid.");

            var authorIds = uow.AuthorRepository.FindAll().Select(x => x.Id);
            var genreIds = uow.GenreRepository.FindAll().Select(x => x.Id);
            if (model.AuthorsIds.Except(authorIds).Any())
                throw new CardFileInvalidDataException("invalid authors ids.");
            if (model.GenresIds.Except(genreIds).Any())
                throw new CardFileInvalidDataException("invalid genres ids.");
        }

        /// <summary>
        /// adds new text material asynchronously
        /// </summary>
        /// <param name="model">text material to add</param>
        /// <returns>Task</returns>
        public async Task AddAsync(AddTextMaterialModel model)
        {
            CheckAddText(model);
            model.AuthorsIds = model.AuthorsIds.Distinct().ToList();
            model.GenresIds = model.GenresIds.Distinct().ToList();
            try
            {
                await uow.TextMaterialRepository.AddAsync(mapper.Map<TextMaterial>(model));
                await uow.SaveAsync();
            }
            catch
            {
                throw new CardFileInvalidDataException("This text is already added.");
            }
        }

        /// <summary>
        /// deletes text material by id asynchronously
        /// </summary>
        /// <param name="modelId">id of text material that shoud be deleted</param>
        /// <returns>Task</returns>
        public async Task DeleteByIdAsync(int modelId)
        {
            var n = uow.TextMaterialRepository.GetByIdAsync(modelId).Result;
            if (n == null)
                throw new CardFileNotFoundException($"Text material with id {modelId} was not found");
            await uow.TextMaterialRepository.DeleteByIdAsync(modelId);
            await uow.SaveAsync();
        }

        /// <summary>
        /// returns collection of all text materials with details
        /// </summary>
        /// <returns>Collection of text materials</returns>
        public IEnumerable<GetTextMaterialModel> GetAll()
        {
            return mapper.Map<IEnumerable<GetTextMaterialModel>>(uow.TextMaterialRepository.GetAllWithDetails());
        }

        /// <summary>
        /// returns all text materials with specified genre name
        /// </summary>
        /// <param name="genre">genre name for text material filtering</param>
        /// <returns>collection of text materials that have specified genre</returns>
        public IEnumerable<GetTextMaterialModel> GetAllWithGenre(string genre)
        {
            var allGenres = (IEnumerable<Genre>)uow.GenreRepository.FindAll();
            var allTexts = (IEnumerable<TextMaterial>)uow.TextMaterialRepository.GetAllWithDetails();

            var gen = allGenres.FirstOrDefault(g => g.Name == genre);
            if (gen == null)
                throw new CardFileNotFoundException($"Genre {genre} was not found");
            var texts = allTexts.Where(t => t.GenreTextMaterials.Exists(x => x.GenreId == gen.Id));

            return mapper.Map<IEnumerable<GetTextMaterialModel>>(texts);
        }


        /// <summary>
        ///  returns all text materials with specified author name and surname
        /// </summary>
        /// <param name="name"> author name</param>
        /// <param name="surname"> author surname</param>
        /// <returns>collection of text materials that have specified author</returns>
        public IEnumerable<GetTextMaterialModel> GetAllWithAuthor(string name, string surname)
        {
            var allAuthors = (IEnumerable<Author>)uow.AuthorRepository.FindAll();
            var allTexts = (IEnumerable<TextMaterial>)uow.TextMaterialRepository.GetAllWithDetails();

            var author = allAuthors.FirstOrDefault(g => g.Name == name && g.Surname == surname);
            if (author == null)
                throw new CardFileNotFoundException($"Author {name} {surname} was not found.");
            var texts = allTexts.Where(t => t.AuthorTextMaterials.Exists(x => x.AuthorId == author.Id));

            return mapper.Map<IEnumerable<GetTextMaterialModel>>(texts);
        }

        /// <summary>
        /// returns text material by id asynchronously
        /// </summary>
        /// <param name="id">id of text material that shoud be returned</param>
        public async Task<GetTextMaterialModel> GetByIdAsync(int id)
        {
            var obj = await uow.TextMaterialRepository.GetByIdAsync(id);
            return mapper.Map<GetTextMaterialModel>(obj);
        }

        /// <summary>
        /// returns text material by id with details asynchronously
        /// </summary>
        /// <param name="id">id of text material that shoud be returned</param>
        public async Task<GetTextMaterialModel> GetByIdWithDetailsAsync(int id)
        {
            return mapper.Map<GetTextMaterialModel>(await uow.TextMaterialRepository.GetByIdWithDetailsAsync(id));
        }

        /// <summary>
        /// updates text material asynchronously
        /// </summary>
        /// <param name="model">model with new values</param>
        public async Task<UpdateTextMaterialModel> UpdateAsync(UpdateTextMaterialModel model)
        {
            var text = uow.TextMaterialRepository.GetByIdWithDetailsAsync(model.Id).Result;

            if (text == null)
                throw new CardFileNotFoundException($"Text material with id {model.Id} was not found");

            var newText = CheckAndUpdateText(model, text);

            uow.TextMaterialRepository.Update(newText);
            await uow.SaveAsync();
            return mapper.Map<UpdateTextMaterialModel>(newText);
        }

        /// <summary>
        /// checks text material model and updates existing text material entity
        /// </summary>
        /// <param name="model">model with new values</param>
        /// <param name="text">existing enity that will be updated</param>
        /// <returns>updated entity</returns>
        public TextMaterial CheckAndUpdateText(UpdateTextMaterialModel model, TextMaterial text)
        {
            if (model.Body != null)
                text.Body = model.Body;
            if (model.Title != null)
                text.Title = model.Title;
            if (model.AuthorsIds != null && model.AuthorsIds.Count != 0)
            {
                var authorIds = uow.AuthorRepository.FindAll().Select(x => x.Id);
                if (model.AuthorsIds.Except(authorIds).Any())
                    throw new CardFileInvalidDataException("invalid authors ids.");
                model.AuthorsIds = model.AuthorsIds.Distinct().ToList();
                text.AuthorTextMaterials = model.AuthorsIds.Select(x => new AuthorTextMaterial 
                { AuthorId = x, TextMaterialId = model.Id }).ToList();
            }
            if (model.GenresIds != null && model.GenresIds.Count != 0)
            {
                var genreIds = uow.GenreRepository.FindAll().Select(x => x.Id);
                if (model.GenresIds.Except(genreIds).Any())
                    throw new CardFileInvalidDataException("invalid genres ids.");
                model.GenresIds = model.GenresIds.Distinct().ToList();
                text.GenreTextMaterials = model.GenresIds.Select(x => new GenreTextMaterial 
                { GenreId = x, TextMaterialId = model.Id }).ToList();
            }
            return text;
        }

        /// <summary>
        /// calculates and updates text material rating
        /// </summary>
        /// <param name="rating">new rating values</param>
        public async Task<GetTextMaterialModel> UpdateRatingAsync(UpdateTextMaterialRating rating)
        {
            var text = uow.TextMaterialRepository.GetByIdWithDetailsAsync(rating.Id).Result;

            if (text == null)
                throw new CardFileNotFoundException($"Text material with id {rating.Id} was not found");

            text.Rating = (text.Rating * text.RatingGradeCount + rating.Rating) / (text.RatingGradeCount + 1);
            text.RatingGradeCount++;
            uow.TextMaterialRepository.Update(text);
            await uow.SaveAsync();
            return mapper.Map<GetTextMaterialModel>(text);
        }
    }
}
