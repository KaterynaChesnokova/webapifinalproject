﻿using AutoMapper;
using Business.Exceptions;
using Business.ServicesInterfaces;
using Data.DataEntities;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services
{
    /// <summary>
    /// Class contains business logic for actions with genre
    /// </summary>
    public class GenreService : IGenreService
    {
        readonly IUnitOfWork uow;
        readonly IMapper mapper;

        public GenreService(IUnitOfWork _uow, IMapper profile)
        {
            uow = _uow;
            mapper = profile;
        }

        /// <summary>
        /// adds new genre asynchronously
        /// </summary>
        /// <param name="model">genre to add</param>
        /// <returns>Task</returns>
        public async Task AddAsync(AddGenreModel model)
        {
            try
            {
                await uow.GenreRepository.AddAsync(mapper.Map<Genre>(model));
                await uow.SaveAsync();
            }
            catch
            {
                throw new CardFileInvalidDataException("This genre is already added.");
            }
        }

        /// <summary>
        /// deletes genre by id asynchronously
        /// </summary>
        /// <param name="modelId">id of genre that shoud be deleted</param>
        /// <returns>Task</returns>
        public async Task DeleteByIdAsync(int modelId)
        {
            var genre = await uow.GenreRepository.GetByIdWithDetailsAsync(modelId);

            if (genre == null)
                throw new CardFileNotFoundException($"genre with id {modelId} was not found.");

            if (genre.GenreTextMaterials.Count != 0)
                throw new CardFileInvalidDataException("genre cannot be deleted because there are already text materials related to it.");

            uow.GenreRepository.Delete(genre);
            await uow.SaveAsync();
        }

        /// <summary>
        /// returns collection of all genres with details
        /// </summary>
        /// <returns>Collection of genres</returns>
        public IEnumerable<GetGenreModel> GetAll()
        {
            return mapper.Map<IEnumerable<GetGenreModel>>(uow.GenreRepository.GetAllWithDetails());
        }

        /// <summary>
        /// returns genre by id asynchronously
        /// </summary>
        /// <param name="id">id of genre that shoud be returned</param>
        public async Task<GetGenreModel> GetByIdAsync(int id)
        {
            return mapper.Map<GetGenreModel>(await uow.GenreRepository.GetByIdAsync(id));
        }

        /// <summary>
        /// returns genre by id with details asynchronously
        /// </summary>
        /// <param name="id">id of genre that shoud be returned</param>
        public async Task<GetGenreModel> GetByIdWithDetailsAsync(int id)
        {
            var obj = await uow.GenreRepository.GetByIdWithDetailsAsync(id);
            return mapper.Map<GetGenreModel>(obj);
        }
    }

}
