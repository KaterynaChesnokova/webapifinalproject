﻿using AutoMapper;
using Business.Exceptions;
using Business.Models;
using Business.ServicesInterfaces;
using Data.DataEntities;
using Data.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services
{
    /// <summary>
    /// Class contains business logic for actions with user
    /// </summary>
    public class UserService : IUserService
    {
        readonly IUnitOfWork uow;
        readonly IMapper mapper;

        public UserService(IUnitOfWork _uow, IMapper profile)
        {
            uow = _uow;
            mapper = profile;
        }

        /// <summary>
        /// fins user by his id asynchronously
        /// </summary>
        /// <param name="id">user id</param>
        public async Task<UserModel> UserFindByIdAsync(string id)
        {
            return mapper.Map<UserModel>(await uow.UserRepository.UserFindByIdAsync(id));
        }

        /// <summary>
        /// fins user by his name asynchronously
        /// </summary>
        /// <param name="id">user name</param>
        public async Task<UserModel> UserFindByNameAsync(string name)
        {
            return mapper.Map<UserModel>(await uow.UserRepository.UserFindByNameAsync(name));
        }

        /// <summary>
        /// fins user by his email asynchronously
        /// </summary>
        /// <param name="id">user email</param>
        public async Task<UserModel> UserFindByEmailAsync(string email)
        {
            return mapper.Map<UserModel>(await uow.UserRepository.UserFindByEmailAsync(email));
        }

        /// <summary>
        /// checks user password
        /// </summary>
        /// <param name="user">user who enters password</param>
        /// <param name="password">user password</param>
        public async Task<bool> UserCheckPasswordAsync(UserModel user, string password)
        {
            return await uow.UserRepository.UserCheckPasswordAsync(mapper.Map<User>(user), password);
        }

        /// <summary>
        /// returns user roles
        /// </summary>
        /// <param name="user">user for getting roles</param>
        /// <returns>list of user roles</returns>
        public async Task<IList<string>> UserGetRolesAsync(UserModel user)
        {
            return await uow.UserRepository.UserGetRolesAsync(mapper.Map<User>(user));
        }

        /// <summary>
        /// creates new user
        /// </summary>
        /// <param name="user">new user</param>
        /// <param name="password">new user password</param>
        public async Task<IdentityResult> UserCreateAsync(UserModel user, string password)
        {
            return await uow.UserRepository.UserCreateAsync(mapper.Map<User>(user), password);
        }

        /// <summary>
        /// checks if user has specified role
        /// </summary>
        /// <param name="roleName">name of the role</param>
        /// <returns>true if user has specified role, otherwise - false</returns>
        public async Task<bool> RoleExistsAsync(string roleName)
        {
            return await uow.UserRepository.RoleExistsAsync(roleName);
        }

        /// <summary>
        /// creates new role
        /// </summary>
        /// <param name="role">new role</param>
        public async Task<IdentityResult> RoleCreateAsync(RoleModel role)
        {
            return await uow.UserRepository.CreateAsync(mapper.Map<IdentityRole>(role));
        }

        /// <summary>
        /// adds new role to user
        /// </summary>
        /// <param name="user">user to add new role</param>
        /// <param name="role">role name</param>
        public async Task<IdentityResult> AddToRoleAsync(UserModel user, string role)
        {
            return await uow.UserRepository.AddToRoleAsync(mapper.Map<User>(user), role);
        }

        /// <summary>
        /// creates new user and adds him to role
        /// </summary>
        /// <param name="user">created user</param>
        /// <param name="password">user password</param>
        /// <param name="role">role name</param>
        public async Task<IdentityResult> CreateAndAddToRole(UserModel user, string password, string role)
        {
            return await uow.UserRepository.CreateAndAddToRole(mapper.Map<User>(user), password, role);
        }

        /// <summary>
        /// removes user from database
        /// </summary>
        /// <param name="userName">user name</param>
        public async Task<IdentityResult> UserDeleteAccountAsync(string userName)
        {
            var user = await uow.UserRepository.UserFindByNameAsync(userName);
            if (user == null)
                throw new CardFileNotFoundException("User is not defined.");

            var defaultUser = await uow.UserRepository.UserFindByNameAsync("Deleted User");

            foreach (var text in user.Texts)
                text.Publisher = defaultUser;

            await uow.SaveAsync();
            return await uow.UserRepository.UserDeleteAccountAsync(userName);
        }
    }
}