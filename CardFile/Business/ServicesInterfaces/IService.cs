﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Business.ServicesInterfaces
{
    public interface IService<TModel> where TModel : class
    {
        IEnumerable<TModel> GetAll();

        Task<TModel> GetByIdAsync(int id);

        Task AddAsync(TModel model);

        Task UpdateAsync(TModel model);

        Task DeleteByIdAsync(int modelId);
    }
}
