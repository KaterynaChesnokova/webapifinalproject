﻿using Business.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.ServicesInterfaces
{
    public interface IAuthorService
    {
        Task AddAsync(AddAuthorModel model);
        Task DeleteByIdAsync(int modelId);
        IEnumerable<GetAuthorModel> GetAll();
        Task<GetAuthorModel> GetByIdAsync(int id);
        Task<GetAuthorModel> GetByIdWithDetailsAsync(int id);
    }
}