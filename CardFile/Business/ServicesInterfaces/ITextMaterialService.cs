﻿using Business.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.ServicesInterfaces
{
    public interface ITextMaterialService
    {
        Task AddAsync(AddTextMaterialModel model);
        Task DeleteByIdAsync(int modelId);
        IEnumerable<GetTextMaterialModel> GetAll();
        Task<GetTextMaterialModel> GetByIdAsync(int id);
        Task<UpdateTextMaterialModel> UpdateAsync(UpdateTextMaterialModel model);
        Task<GetTextMaterialModel> GetByIdWithDetailsAsync(int id);
        IEnumerable<GetTextMaterialModel> GetAllWithGenre(string genre);
        IEnumerable<GetTextMaterialModel> GetAllWithAuthor(string name, string surname);
        Task<GetTextMaterialModel> UpdateRatingAsync(UpdateTextMaterialRating rating);
    }
}