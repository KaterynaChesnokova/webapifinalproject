﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.ServicesInterfaces
{
    public interface IGenreService
    {
        Task AddAsync(AddGenreModel model);
        Task DeleteByIdAsync(int modelId);
        IEnumerable<GetGenreModel> GetAll();
        Task<GetGenreModel> GetByIdAsync(int id);
        Task<GetGenreModel> GetByIdWithDetailsAsync(int id);
    }
}