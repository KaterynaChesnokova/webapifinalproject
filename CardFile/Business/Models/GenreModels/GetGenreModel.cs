﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Business
{
    /// <summary>
    /// model for receiving genre
    /// </summary>
    public class GetGenreModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<int> TextsIds { get; set; }
    }
}
