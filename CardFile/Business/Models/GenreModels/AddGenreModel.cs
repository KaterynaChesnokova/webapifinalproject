﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Business
{
    /// <summary>
    /// model for adding new genre
    /// </summary>
   public class AddGenreModel
    {
        [Required]
        [StringLength(30, MinimumLength = 2)]
        public string Name { get; set; }
    }
}
