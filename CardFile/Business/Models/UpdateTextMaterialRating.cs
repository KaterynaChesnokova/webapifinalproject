﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Business.Models
{
    /// <summary>
    /// model for updating text materials rating
    /// </summary>
    public class UpdateTextMaterialRating
    {
        [Range(1, int.MaxValue, ErrorMessage = "Text material id is required")]
        public int Id { get; set; }

        [Required]
        [Range(1, 10)]
        public int Rating { get; set; }
    }
}
