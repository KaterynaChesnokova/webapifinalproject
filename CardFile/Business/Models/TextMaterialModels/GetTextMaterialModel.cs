﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Models
{
    /// <summary>
    /// model for receiving text material
    /// </summary>
    public class GetTextMaterialModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Body { get; set; }

        public DateTime PublishDate { get; set; }

        public double Rating { get; set; }

        public int RatingGradeCount { get; set; }

        public string PublisherId { get; set; }

        public ICollection<int> AuthorsIds { get; set; }
        public ICollection<int> GenresIds { get; set; }
    }
}
