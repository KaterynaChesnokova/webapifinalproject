﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Business.Models
{
    /// <summary>
    /// model for adding new text material
    /// </summary>
    public class AddTextMaterialModel
    {
        [Required]
        [StringLength(50, MinimumLength = 1)]
        public string Title { get; set; }

        [Required]
        [StringLength(1000, MinimumLength = 40)]
        public string Body { get; set; }

        public DateTime PublishDate { get; set; }

        public string PublisherId { get; set; }

        [Required]
        [MinLength(1,ErrorMessage = "text material must have at least one author")]
        public ICollection<int> AuthorsIds { get; set; }

        [Required]
        [MinLength(1, ErrorMessage = "text material must have at least one genre")]
        public ICollection<int> GenresIds { get; set; }
    }
}
