﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Business.Models
{
    /// <summary>
    /// model for updating text material
    /// </summary>
    public class UpdateTextMaterialModel
    {
        [Range(1, int.MaxValue, ErrorMessage = "Text material id is required")]
        public int Id { get; set; }

        public string Title { get; set; }

        public string Body { get; set; }

        public ICollection<int> AuthorsIds { get; set; }
        public ICollection<int> GenresIds { get; set; }
    }
}
