﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Models
{
    /// <summary>
    /// model for storing user full user information
    /// </summary>
    public class UserModel
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string SecurityStamp { get; set; }
        public string UserName { get; set; }
        public string PasswordHash { get; set; }
        public string PhoneNumber { get; set; }
        public string NormalizedEmail { get; set; }
        public string NormalizedUserName { get; set; }

        /// <summary>
        /// roles that user has
        /// </summary>
        public IEnumerable<string> Roles { get; set; }

        /// <summary>
        /// text materials that user posted
        /// </summary>
        public ICollection<int> TextsIds { get; set; }
    }
}
