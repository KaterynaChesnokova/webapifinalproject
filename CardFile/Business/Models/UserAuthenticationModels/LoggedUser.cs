﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Models.UserAuthenticationModels
{
    /// <summary>
    /// model for storing logged user information
    /// </summary>
    public class LoggedUser
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Token { get; set; }
        public DateTime Expiration { get; set; }

        public IEnumerable<string> Roles { get; set; }
    }
}
