﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Models
{
    /// <summary>
    /// instance of class is sent when exception is thrown during responce processing
    /// </summary>
    public class ErrorResponse
    {
        public string Type { get; set; }
        public string Message { get; set; }

        public ErrorResponse(Exception ex)
        {
            Type = ex.GetType().Name;
            Message = ex.Message;
        }

        public override string ToString()
        {
            return $"Type: {Type}, Message: {Message}";
        }
    }
}
