﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Business.ServicesInterfaces;
using Microsoft.AspNetCore.Mvc;
using Business;
using Microsoft.AspNetCore.Authorization;

namespace WebApi.Controllers
{
    /// <summary>
    /// controller for genres processing
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class GenresController : ControllerBase
    {
        readonly IGenreService service;

        public GenresController(IGenreService _service)
        {
            service = _service;
        }

        /// <summary>
        /// action thar returns all genres
        /// </summary>
        /// <returns>Ok if any genres were found, NoContent if no genres were found</returns>
        // GET: api/Genres
        [HttpGet]
        public ActionResult<IEnumerable<GetGenreModel>> GetAll()
        {
            var all = service.GetAll();
            if (all.Any())
                return Ok(all);
            return NoContent();
        }

        /// <summary>
        /// action that returns genre by id
        /// </summary>
        /// <param name="id">genre id</param>
        /// <returns>Ok if genre was found, otherwise - NoContent</returns>
        // GET api/Genres/5
        [HttpGet("{id}", Name = "GetGenre")]
        public async Task<ActionResult<GetGenreModel>> GetById(int id)
        {
            var model = await service.GetByIdWithDetailsAsync(id);
            if (model == null)
                return NoContent();
            return Ok(model);
        }

        /// <summary>
        /// action for adding new genre
        /// </summary>
        /// <param name="model">new genre</param>
        /// <returns>Created if new genre were successfully added</returns>
        // POST api/Genres
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Add([FromBody] AddGenreModel model)
        {
            await service.AddAsync(model);
            var addedModel = service.GetAll().First(x => x.Name == model.Name);
            return CreatedAtRoute("GetGenre", new { id = addedModel.Id }, addedModel);
        }

        /// <summary>
        /// action deletes genre by id
        /// </summary>
        /// <param name="id">id of genre that should be deleted</param>
        /// <returns>Ok if genre was deleted successfully</returns>
        // DELETE api/Genres/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Delete(int id)
        {
            await service.DeleteByIdAsync(id);
            return Ok();
        }
    }
}
