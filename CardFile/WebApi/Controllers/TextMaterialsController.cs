﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Models;
using Business.ServicesInterfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    /// <summary>
    /// controller for text materials processing
    /// </summary>
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TextMaterialsController : ControllerBase
    {
        readonly ITextMaterialService service;
        readonly IUserService userService;

        public TextMaterialsController(ITextMaterialService _service, IUserService _userService)
        {
            service = _service;
            userService = _userService;
        }

        /// <summary>
        /// action thar returns all text materials
        /// </summary>
        /// <returns>Ok if any text materials were found, NoContent if no text materials were found</returns>
        // GET: api/TextMaterials
        [HttpGet]
        [AllowAnonymous]
        public ActionResult<IEnumerable<GetTextMaterialModel>> GetAll()
        {
            var all = service.GetAll();
            if (all.Any())
                return Ok(all);
            return NoContent();
        }

        /// <summary>
        /// action that returns all text materials with specified genre
        /// </summary>
        /// <param name="genre">genre name</param>
        /// <returns>Ok if any text materials with genre were found, NoContent if no text materials were found</returns>
        // GET: api/TextMaterials/withgenre/Detective
        [HttpGet("withgenre/{genre}")]
        [AllowAnonymous]
        public ActionResult<IEnumerable<GetTextMaterialModel>> GetAllWithGenre(string genre)
        {
            var texts = service.GetAllWithGenre(genre);
            if (texts.Any())
                return Ok(texts);
            return NoContent();
        }

        /// <summary>
        /// action that returns all text materials with specified author
        /// </summary>
        /// <param name="name">author name</param>
        /// <param name="surname">author surname</param>
        /// <returns>Ok if any text materials with author were found, NoContent if no text materials were found</returns>
        // GET: api/TextMaterials/withauthor/Richard/White
        [HttpGet("withauthor/{name}/{surname}")]
        [AllowAnonymous]
        public ActionResult<IEnumerable<GetTextMaterialModel>> GetAllWithAuthor(string name, string surname)
        {
            var texts = service.GetAllWithAuthor(name, surname);
            if (texts.Any())
                return Ok(texts);
            return NoContent();
        }

        /// <summary>
        /// action that returns text material by id
        /// </summary>
        /// <param name="id">text material id</param>
        /// <returns>Ok if text material was found, otherwise - NoContent</returns>
        // GET api/TextMaterials/5
        [HttpGet("{id}", Name = "GetTextMaterial")]
        [AllowAnonymous]
        public async Task<ActionResult<GetTextMaterialModel>> GetById(int id)
        {
            var model = await service.GetByIdWithDetailsAsync(id);
            if (model == null)
                return NoContent();
            return Ok(model);
        }

        /// <summary>
        /// action for adding new text material
        /// </summary>
        /// <param name="model">new text material</param>
        /// <returns>Created if new text material were successfully added</returns>
        // POST api/TextMaterials
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Add([FromBody] AddTextMaterialModel model)
        {
            var userId = User.Identity.Name;
            var user = await userService.UserFindByIdAsync(userId);
            model.PublisherId = user.Id;
            model.PublishDate = DateTime.Now;
            await service.AddAsync(model);
            var addedModel = service.GetAll().First(x => x.Body == model.Body && x.Title == model.Title);
            return CreatedAtRoute("GetTextMaterial", new { id = addedModel.Id }, addedModel);
        }

        /// <summary>
        /// action for text material updating
        /// </summary>
        /// <param name="model">model with new values</param>
        /// <returns>Ok if text material was updated successfully</returns>
        // PUT api/TextMaterials
        [HttpPut]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Update([FromBody] UpdateTextMaterialModel model)
        {
            var updated = await service.UpdateAsync(model);
            return Ok(updated);
        }

        /// <summary>
        /// action for text material rating updating
        /// </summary>
        /// <param name="rating">model with rating values</param>
        /// <returns>Ok if rating was updated successfully</returns>
        // PUT api/TextMaterials/rate
        [HttpPut("rate")]
        [Authorize(Roles = "User")]
        public async Task<ActionResult> UpdateRating([FromBody] UpdateTextMaterialRating rating)
        {
            var updated = await service.UpdateRatingAsync(rating);
            return Ok(updated);
        }

        /// <summary>
        /// action deletes text material by id
        /// </summary>
        /// <param name="id">id of text material that should be deleted</param>
        /// <returns>Ok if text material was deleted successfully</returns>
        // DELETE api/TextMaterials/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Delete(int id)
        {
            await service.DeleteByIdAsync(id);
            return Ok();
        }
    }
}
