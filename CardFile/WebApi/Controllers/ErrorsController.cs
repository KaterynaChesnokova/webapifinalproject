﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business;
using Business.Exceptions;
using Business.Models;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace WebApi.Controllers
{
    /// <summary>
    /// controller for error handling
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ErrorsController : ControllerBase
    {
        /// <summary>
        /// logger that logs exceptions
        /// </summary>
        readonly ILogger<ErrorsController> logger;

        public ErrorsController(ILogger<ErrorsController> _logger)
        {
            logger = _logger;
        }

        /// <summary>
        /// method for error response generation
        /// </summary>
        /// <returns>error response</returns>
        [Route("error")]
        public ErrorResponse Error()
        {
            var context = HttpContext.Features.Get<IExceptionHandlerFeature>();
            var exception = context?.Error;
            var code = 500;

            if (exception is CardFileNotFoundException)
                code = 400;
            else if (exception is CardFileInvalidDataException)
                code = 400;

            Response.StatusCode = code;

            var response = new ErrorResponse(exception);
            return response;
        }
    }
}
