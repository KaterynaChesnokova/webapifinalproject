﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Business.Models;
using Business.Models.UserAuthenticationModels;
using Business.ServicesInterfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace WebApi.Controllers
{
    /// <summary>
    /// controller for authentication
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticateController : ControllerBase
    {
        readonly IConfiguration configuration;
        readonly IUserService service;

        public AuthenticateController(IConfiguration _configuration, IUserService _service)
        {
            configuration = _configuration;
            service = _service;
        }

        /// <summary>
        /// action for user login
        /// </summary>
        /// <param name="model">user login model with email and password</param>
        /// <returns>Ok if user is authorised successfully, otherwise - Unauthorized</returns>
        [HttpPost]
        [Route("login", Name = "Login")]
        public async Task<ActionResult<LoggedUser>> Login([FromBody] LoginModel model)
        {
            var user = await service.UserFindByEmailAsync(model.Email);
            if (user != null && await service.UserCheckPasswordAsync(user, model.Password))
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var authSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(configuration["JWT:Secret"]));

                var userRoles = await service.UserGetRolesAsync(user);
                var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim(ClaimTypes.Name, user.UserName),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                };

                foreach (var userRole in userRoles)
                {
                    authClaims.Add(new Claim(ClaimTypes.Role, userRole));
                }


                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(authClaims),
                    Expires = DateTime.UtcNow.AddHours(5),
                    SigningCredentials = new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256Signature)
                };

                var token = tokenHandler.CreateToken(tokenDescriptor);

                return Ok(new LoggedUser
                {
                    Id = user.Id,
                    UserName = user.UserName,
                    Roles = userRoles,
                    Token = new JwtSecurityTokenHandler().WriteToken(token),
                    Expiration = token.ValidTo
                });
            }
            return Unauthorized();
        }

        /// <summary>
        /// actio for user registration
        /// </summary>
        /// <param name="model">model with registration information</param>
        /// <returns>BadRequest if user is already exists, Ok if user is authorised successfully, otherwise - Unauthorized</returns>
        [HttpPost]
        [Route("register")]
        public async Task<ActionResult<LoggedUser>> Register([FromBody] RegisterModel model)
        {
            var emailExists = await service.UserFindByEmailAsync(model.Email);
            if (emailExists != null)
                return BadRequest("User with such email already exists");
            var nameExists = await service.UserFindByNameAsync(model.UserName);
            if (nameExists != null)
                return BadRequest("User with such name already exists");

            UserModel user = new UserModel()
            {
                Email = model.Email,
                SecurityStamp = Guid.NewGuid().ToString(),
                UserName = model.UserName
            };

            if (!await service.RoleExistsAsync("User"))
                await service.RoleCreateAsync(new RoleModel { Name = "User" });

            await service.CreateAndAddToRole(user, model.Password, "User");

            return await Login(new LoginModel { Email = model.Email, Password = model.Password });
        }

        /// <summary>
        /// action for new admin registration
        /// </summary>
        /// <param name="model">model with registration information</param>
        /// <returns>BadRequest if user is already exists, Ok if user is authorised successfully</returns>
        [HttpPost]
        [Route("register-admin")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> RegisterAdmin([FromBody] RegisterModel model)
        {
            var emailExists = await service.UserFindByEmailAsync(model.Email);
            if (emailExists != null)
                return BadRequest("User with such email already exists");
            var nameExists = await service.UserFindByNameAsync(model.UserName);
            if (nameExists != null)
                return BadRequest("User with such name already exists");

            UserModel user = new UserModel()
            {
                Email = model.Email,
                SecurityStamp = Guid.NewGuid().ToString(),
                UserName = model.Email
            };

            if (!await service.RoleExistsAsync("Admin"))
                await service.RoleCreateAsync(new RoleModel { Name = "Admin" });
            if (!await service.RoleExistsAsync("User"))
                await service.RoleCreateAsync(new RoleModel { Name = "User" });

            await service.CreateAndAddToRole(user, model.Password, "Admin");

            return Ok();
        }

        /// <summary>
        /// action returns all user roles
        /// </summary>
        /// <param name="user">user to find roles for</param>
        /// <returns>BadRequest if user is undefined, Ok if roles found</returns>
        [HttpPut]
        [Route("getroles")]
        public async Task<ActionResult<IEnumerable<RoleModel>>> GetUserRoles(UserModel user)
        {
            if (user == default(UserModel))
                return BadRequest();
            var roles = await service.UserGetRolesAsync(user);
            return Ok(roles);
        }

        /// <summary>
        /// action for account deletion
        /// </summary>
        /// <returns>Ok if account was successfully deleted</returns>
        [HttpDelete]
        [Route("delete")]
        [Authorize]
        public async Task<IActionResult> DeleteAccount()
        {
            var name = User.Identity.Name;
            await service.UserDeleteAccountAsync(name);
            return Ok();
        }
    }
}
