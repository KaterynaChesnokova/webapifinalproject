﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Models;
using Business.ServicesInterfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    /// <summary>
    /// controller for authors processing
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorsController : ControllerBase
    {
        readonly IAuthorService service;

        public AuthorsController(IAuthorService _service)
        {
            service = _service;
        }

        /// <summary>
        /// action thar returns all authors
        /// </summary>
        /// <returns>Ok if any authors were found, NoContent if no authors were found</returns>
        // GET: api/Authors
        [HttpGet]
        public ActionResult<IEnumerable<GetAuthorModel>> GetAll()
        {
            var all = service.GetAll();
            if (all.Any())
                return Ok(all);
            return NoContent();
        }

        /// <summary>
        /// action that returns author by id
        /// </summary>
        /// <param name="id">author id</param>
        /// <returns>Ok if author was found, otherwise - NoContent</returns>
        // GET api/Authors/5
        [HttpGet("{id}", Name = "GetAuthor")]
        public async Task<ActionResult<GetAuthorModel>> GetById(int id)
        {
            var model = await service.GetByIdWithDetailsAsync(id);
            if (model == null)
                return NoContent();
            return Ok(model);
        }

        /// <summary>
        /// action for adding new author
        /// </summary>
        /// <param name="model">new author</param>
        /// <returns>Created if new author were successfully added</returns>
        // POST api/Authors
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Add([FromBody] AddAuthorModel model)
        {
            await service.AddAsync(model);
            var addedModel = service.GetAll().First(x => x.Name == model.Name && x.Surname == model.Surname);
            return CreatedAtRoute("GetAuthor", new { id = addedModel.Id }, addedModel);
        }

        /// <summary>
        /// action deletes author by id
        /// </summary>
        /// <param name="id">id of author that should be deleted</param>
        /// <returns>Ok if author was deleted successfully</returns>
        // DELETE api/Authors/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Delete(int id)
        {
            await service.DeleteByIdAsync(id);
            return Ok();
        }
    }
}
