﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class MoreSeedDataAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "0aff50d8-2cad-40da-8db3-493f44af5908", "626b79c9-b1f2-4bf2-ab50-b91dd01f98fb" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "b7bbad19-1687-4233-9372-04460b911878", "626b79c9-b1f2-4bf2-ab50-b91dd01f98fb" });

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "e52f5d0b-f71a-4457-a9c7-f04bfc7cd730");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "0aff50d8-2cad-40da-8db3-493f44af5908");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b7bbad19-1687-4233-9372-04460b911878");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "626b79c9-b1f2-4bf2-ab50-b91dd01f98fb");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "d87740fa-a3c4-4e75-9d06-ef555f6712ec", "ab50ed0e-5ac0-49d7-ad97-1cc1013848fd", "Admin", "ADMIN" },
                    { "dee13ae5-06f6-4eca-9a6b-f18890b8ee92", "944e0dc7-ac70-4a88-bc06-d322fcc289a9", "User", "USER" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { "475d6969-511e-4169-9ee4-4d2acc1de191", 0, "e057eba7-2a40-45b0-a7bd-7dccbf79f1b3", "admin@gmail.com", false, false, null, "ADMIN@GMAIL.COM", "ADMIN", "AQAAAAEAACcQAAAAEE7nWckGDJonf0flEApxor6hh9nv17pr/7c3D26l+lGsnLf9OQFYSB/vS6+7CvSgLg==", null, false, "51501854-a3ca-43bb-b237-9b8e3a0ba378", false, "Admin" },
                    { "19d7c79c-374d-44c0-9359-5ffa8840d551", 0, "3ddbc2c8-7bcf-4ece-84f7-8b9547e50ddb", "deleted@gmail.com", false, false, null, "DELETED@GMAIL.COM", "DELETED USER", "AQAAAAEAACcQAAAAEEax9ZRgWoT6rPjBNz6DYKAXv0WapugremJdEhNQ1gNBAWF7rNbd+cMTm/EVUXI/FQ==", null, false, "2c4c3950-93ad-4e21-9538-5122cdb7a7a0", false, "Deleted User" }
                });

            migrationBuilder.InsertData(
                table: "Authors",
                columns: new[] { "Id", "Name", "Surname" },
                values: new object[,]
                {
                    { 6, "Linda", "Kornel" },
                    { 7, "Maria", "Mirabella" }
                });

            migrationBuilder.InsertData(
                table: "Genres",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 7, "Westerns" },
                    { 8, "History" },
                    { 9, "Children’s" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[,]
                {
                    { "d87740fa-a3c4-4e75-9d06-ef555f6712ec", "475d6969-511e-4169-9ee4-4d2acc1de191" },
                    { "dee13ae5-06f6-4eca-9a6b-f18890b8ee92", "475d6969-511e-4169-9ee4-4d2acc1de191" }
                });

            migrationBuilder.UpdateData(
                table: "TextMaterials",
                keyColumn: "Id",
                keyValue: 1,
                column: "PublisherId",
                value: "475d6969-511e-4169-9ee4-4d2acc1de191");

            migrationBuilder.InsertData(
                table: "TextMaterials",
                columns: new[] { "Id", "Body", "PublishDate", "PublisherId", "Rating", "RatingGradeCount", "Title" },
                values: new object[,]
                {
                    { 2, "Halloween (also referred to as All Hollows' Eve) is a holiday that's celebrated in America on 31 October of each year, regardless of what day of the week this date falls on. Although it is rooted in religion, Halloween today is enjoyed mainly because of its decorations, costumes, candy, treats, and general excitement, and furthermore, it is enjoyed by most everyone.Before Halloween, many individuals carve a design into an orange-colored pumpkin, or a solid, durable vegetable. Once a personally satisfying design is carved, a lit candle is typically put inside a pumpkin, thereby making it a Jack-O-Lantern. At night, this design lights up against the darkness.", new DateTime(2020, 9, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), "475d6969-511e-4169-9ee4-4d2acc1de191", 0.0, 0, "Halloween" },
                    { 3, "London is a famous and historic city. It is the capital of England in the United Kingdom. The city is quite popular for international tourism because London is home to one of the oldest-standing monarchies in the western hemisphere. Rita and Joanne recently traveled to London. They were very excited for their trip because this was their first journey overseas from the United States.Among the popular sights that Rita and Joanne visited are Big Ben, Buckingham Palace, and the London Eye. Big Ben is one of London’s most famous monuments. It is a large clock tower located at the northern end of Westminster Palace. The clock tower is 96 meters tall.", new DateTime(2001, 7, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "475d6969-511e-4169-9ee4-4d2acc1de191", 0.0, 0, "London" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "d87740fa-a3c4-4e75-9d06-ef555f6712ec", "475d6969-511e-4169-9ee4-4d2acc1de191" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "dee13ae5-06f6-4eca-9a6b-f18890b8ee92", "475d6969-511e-4169-9ee4-4d2acc1de191" });

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "19d7c79c-374d-44c0-9359-5ffa8840d551");

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "TextMaterials",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "TextMaterials",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "d87740fa-a3c4-4e75-9d06-ef555f6712ec");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "dee13ae5-06f6-4eca-9a6b-f18890b8ee92");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "475d6969-511e-4169-9ee4-4d2acc1de191");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "b7bbad19-1687-4233-9372-04460b911878", "2336d1b6-a085-48e6-860b-7e0b13691510", "Admin", "ADMIN" },
                    { "0aff50d8-2cad-40da-8db3-493f44af5908", "b07dfec1-ed29-4b57-97f8-a4b877995660", "User", "USER" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { "626b79c9-b1f2-4bf2-ab50-b91dd01f98fb", 0, "b935b5b3-e5b0-4921-a2f2-51f48040f710", "admin@gmail.com", false, false, null, "ADMIN@GMAIL.COM", "ADMIN", "AQAAAAEAACcQAAAAEO+mY3dG1FjsqU0e2EvHSCyXJ0uaKYRSLAKHUjPBwaAy3aWjtDbUMrW8scAx5jYVVA==", null, false, "23920b95-0fd3-41fc-9f33-07f2263fc871", false, "Admin" },
                    { "e52f5d0b-f71a-4457-a9c7-f04bfc7cd730", 0, "87599ff4-5193-47e9-9a0a-5527bcf49910", "deleted@gmail.com", false, false, null, "DELETED@GMAIL.COM", "DELETED USER", "AQAAAAEAACcQAAAAEClsc93P/MRpwiPEQG3NRXctLL6VuiUaOt37LjayFx6hvBZmtAN8GEkDZCGXdh1AXw==", null, false, "041ad3d6-0a77-4562-875d-2f1c5053cb58", false, "Deleted User" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "b7bbad19-1687-4233-9372-04460b911878", "626b79c9-b1f2-4bf2-ab50-b91dd01f98fb" });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "0aff50d8-2cad-40da-8db3-493f44af5908", "626b79c9-b1f2-4bf2-ab50-b91dd01f98fb" });

            migrationBuilder.UpdateData(
                table: "TextMaterials",
                keyColumn: "Id",
                keyValue: 1,
                column: "PublisherId",
                value: "626b79c9-b1f2-4bf2-ab50-b91dd01f98fb");
        }
    }
}
