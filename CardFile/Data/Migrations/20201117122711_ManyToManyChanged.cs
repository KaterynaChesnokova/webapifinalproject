﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class ManyToManyChanged : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AuthorTextMaterial");

            migrationBuilder.DropTable(
                name: "GenreTextMaterial");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "9e788238-6714-401f-8235-7831d535bfed");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "dee72389-2ec8-429b-8dc2-10536a8af568");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2f87a355-dc64-4777-9bb3-71ecb72ee89d");

            migrationBuilder.AddColumn<string>(
                name: "Surname",
                table: "Authors",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "AuthorTextMaterials",
                columns: table => new
                {
                    AuthorId = table.Column<int>(type: "int", nullable: false),
                    TextMaterialId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuthorTextMaterials", x => new { x.AuthorId, x.TextMaterialId });
                    table.ForeignKey(
                        name: "FK_AuthorTextMaterials_Authors_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Authors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AuthorTextMaterials_TextMaterials_TextMaterialId",
                        column: x => x.TextMaterialId,
                        principalTable: "TextMaterials",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GenreTextMaterials",
                columns: table => new
                {
                    GenreId = table.Column<int>(type: "int", nullable: false),
                    TextMaterialId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GenreTextMaterials", x => new { x.GenreId, x.TextMaterialId });
                    table.ForeignKey(
                        name: "FK_GenreTextMaterials_Genres_GenreId",
                        column: x => x.GenreId,
                        principalTable: "Genres",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GenreTextMaterials_TextMaterials_TextMaterialId",
                        column: x => x.TextMaterialId,
                        principalTable: "TextMaterials",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "e82baad0-55b1-49e6-9a76-8e27fc33da10", "d10e0e28-dbb4-4ec7-a539-490dd17e491c", "Admin", "ADMIN" },
                    { "8feb5f6d-4c69-4946-a2f2-c42b8f0077df", "fd89c6a0-3da1-43ef-8fa5-390450fa8ca9", "User", "USER" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { "3830e099-6e8e-4341-b1b8-8136ebce5057", 0, "8125e5fe-6276-48bd-8d32-9d38407f978e", "admin@gmail.com", false, false, null, "ADMIN@GMAIL.COM", "ADMIN@GMAIL.COM", "AQAAAAEAACcQAAAAEEhQ8XaCYWOobtVp4v2ocaZRA509EOdxKL1113VANOcWgtyWNgZuMi8TpqhIS1Jtyw==", null, false, "d708940b-ed97-45bb-9b73-2883e8aff2e4", false, "admin@gmail.com" });

            migrationBuilder.InsertData(
                table: "Authors",
                columns: new[] { "Id", "Name", "Surname" },
                values: new object[,]
                {
                    { 1, "Richard", "White" },
                    { 2, "Oleg", "Zigulia" },
                    { 3, "Olga", "Kustova" },
                    { 4, "Nastia", "Ponomarenko" },
                    { 5, "Mike", "Black" }
                });

            migrationBuilder.InsertData(
                table: "Genres",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Detective" },
                    { 2, "Horror" },
                    { 3, "Comedy" },
                    { 4, "Fantasy" },
                    { 5, "Adventure" },
                    { 6, "Romance" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "e82baad0-55b1-49e6-9a76-8e27fc33da10", "3830e099-6e8e-4341-b1b8-8136ebce5057" });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "8feb5f6d-4c69-4946-a2f2-c42b8f0077df", "3830e099-6e8e-4341-b1b8-8136ebce5057" });

            migrationBuilder.InsertData(
                table: "TextMaterials",
                columns: new[] { "Id", "Body", "PublishDate", "PublisherId", "Rating", "RatingGradeCount", "Title" },
                values: new object[] { 1, "Christina visited Miami during her winter vacation. She is from Boston, where it is cold during the winter months. Miami, however, has a very warm climate. There are many sunny days in Miami, and people can go to the beach all year long. Christina spent a good portion of her trip on the beach to relax and sunbathe. However, she also explored Miami and its surroundings.Inspired by Miami’s proximity to the ocean,Christina visited the Miami Seaquarium to learn about marine life.There,she watched a show using trained dolphins, killer whales, and other aquatic mammals. She took a lot of pictures of the sea creatures jumping out of the water and performing tricks.", new DateTime(2011, 12, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "3830e099-6e8e-4341-b1b8-8136ebce5057", 0.0, 0, "Miami" });

            migrationBuilder.CreateIndex(
                name: "IX_AuthorTextMaterials_TextMaterialId",
                table: "AuthorTextMaterials",
                column: "TextMaterialId");

            migrationBuilder.CreateIndex(
                name: "IX_GenreTextMaterials_TextMaterialId",
                table: "GenreTextMaterials",
                column: "TextMaterialId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AuthorTextMaterials");

            migrationBuilder.DropTable(
                name: "GenreTextMaterials");

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "8feb5f6d-4c69-4946-a2f2-c42b8f0077df", "3830e099-6e8e-4341-b1b8-8136ebce5057" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "e82baad0-55b1-49e6-9a76-8e27fc33da10", "3830e099-6e8e-4341-b1b8-8136ebce5057" });

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "TextMaterials",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "8feb5f6d-4c69-4946-a2f2-c42b8f0077df");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "e82baad0-55b1-49e6-9a76-8e27fc33da10");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "3830e099-6e8e-4341-b1b8-8136ebce5057");

            migrationBuilder.DropColumn(
                name: "Surname",
                table: "Authors");

            migrationBuilder.CreateTable(
                name: "AuthorTextMaterial",
                columns: table => new
                {
                    AuthorsId = table.Column<int>(type: "int", nullable: false),
                    TextsId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuthorTextMaterial", x => new { x.AuthorsId, x.TextsId });
                    table.ForeignKey(
                        name: "FK_AuthorTextMaterial_Authors_AuthorsId",
                        column: x => x.AuthorsId,
                        principalTable: "Authors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AuthorTextMaterial_TextMaterials_TextsId",
                        column: x => x.TextsId,
                        principalTable: "TextMaterials",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GenreTextMaterial",
                columns: table => new
                {
                    GenresId = table.Column<int>(type: "int", nullable: false),
                    TextsId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GenreTextMaterial", x => new { x.GenresId, x.TextsId });
                    table.ForeignKey(
                        name: "FK_GenreTextMaterial_Genres_GenresId",
                        column: x => x.GenresId,
                        principalTable: "Genres",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GenreTextMaterial_TextMaterials_TextsId",
                        column: x => x.TextsId,
                        principalTable: "TextMaterials",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "9e788238-6714-401f-8235-7831d535bfed", "e158444a-332f-4f37-b5c1-5484421ed196", "Admin", "ADMIN" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "dee72389-2ec8-429b-8dc2-10536a8af568", "b2f31431-4c25-4307-9168-356badf96cf3", "User", "USER" });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { "2f87a355-dc64-4777-9bb3-71ecb72ee89d", 0, "7506f68b-390e-4db1-b3bd-4377b7d2de75", "admin@gmail.com", false, false, null, "ADMIN@GMAIL.COM", "SUPERADMIN", "AQAAAAEAACcQAAAAEKZECIbdxHU4kA9Zs1XFM986LiCF4xt4TK26tgb33op9b6trEISns99hQ/jWinmsoQ==", null, false, "f2dee034-0042-4ed4-9a6e-f211dadfeb00", false, "SuperAdmin" });

            migrationBuilder.CreateIndex(
                name: "IX_AuthorTextMaterial_TextsId",
                table: "AuthorTextMaterial",
                column: "TextsId");

            migrationBuilder.CreateIndex(
                name: "IX_GenreTextMaterial_TextsId",
                table: "GenreTextMaterial",
                column: "TextsId");
        }
    }
}
