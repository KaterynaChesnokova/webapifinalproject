﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class DataTypesChanged : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "d87740fa-a3c4-4e75-9d06-ef555f6712ec", "475d6969-511e-4169-9ee4-4d2acc1de191" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "dee13ae5-06f6-4eca-9a6b-f18890b8ee92", "475d6969-511e-4169-9ee4-4d2acc1de191" });

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "19d7c79c-374d-44c0-9359-5ffa8840d551");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "d87740fa-a3c4-4e75-9d06-ef555f6712ec");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "dee13ae5-06f6-4eca-9a6b-f18890b8ee92");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "475d6969-511e-4169-9ee4-4d2acc1de191");

            migrationBuilder.AlterColumn<decimal>(
                name: "Rating",
                table: "TextMaterials",
                type: "decimal(3,2)",
                precision: 3,
                scale: 2,
                nullable: false,
                oldClrType: typeof(double),
                oldType: "float");

            migrationBuilder.AlterColumn<DateTime>(
                name: "PublishDate",
                table: "TextMaterials",
                type: "date",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "0850d92b-1646-4dee-a398-ccb777cad5d1", "eb4ab434-08a9-4ab3-9640-b238cba99329", "Admin", "ADMIN" },
                    { "7260f5f8-39c3-40bd-b1a1-45bfd741cab2", "9bae46e7-1f73-48cc-b813-2b44ddbf4112", "User", "USER" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { "d8d30e37-0411-4a28-bb20-b197ce3f3d44", 0, "b3ccd9dc-436f-4e3c-a2d5-7ad9d5bc9c19", "admin@gmail.com", false, false, null, "ADMIN@GMAIL.COM", "ADMIN", "AQAAAAEAACcQAAAAEJyrmt/FeWTVgEf6DB+3q7/cAazAggql1v2BGXnMGuv9/sQPqU5uUEfKJT5ScgHHjg==", null, false, "4c8a983a-3900-4877-8394-ce85844f7e86", false, "Admin" },
                    { "3d9affc7-a917-4a35-97ad-10f049244077", 0, "420c66ab-34f7-43df-ba67-e12a42a35e40", "deleted@gmail.com", false, false, null, "DELETED@GMAIL.COM", "DELETED USER", "AQAAAAEAACcQAAAAELekJ1URWe7pGq/OJI6nAJXwgFl/8Wa5ACO7LSaEBokIc9GaV3XzkVf1dHkZxTNkXA==", null, false, "14d3cff9-d700-43e7-b02f-80803eac5ad6", false, "Deleted User" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[,]
                {
                    { "0850d92b-1646-4dee-a398-ccb777cad5d1", "d8d30e37-0411-4a28-bb20-b197ce3f3d44" },
                    { "7260f5f8-39c3-40bd-b1a1-45bfd741cab2", "d8d30e37-0411-4a28-bb20-b197ce3f3d44" }
                });

            migrationBuilder.UpdateData(
                table: "TextMaterials",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "PublisherId", "Rating" },
                values: new object[] { "d8d30e37-0411-4a28-bb20-b197ce3f3d44", 0m });

            migrationBuilder.UpdateData(
                table: "TextMaterials",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "PublisherId", "Rating" },
                values: new object[] { "d8d30e37-0411-4a28-bb20-b197ce3f3d44", 0m });

            migrationBuilder.UpdateData(
                table: "TextMaterials",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "PublisherId", "Rating" },
                values: new object[] { "d8d30e37-0411-4a28-bb20-b197ce3f3d44", 0m });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "0850d92b-1646-4dee-a398-ccb777cad5d1", "d8d30e37-0411-4a28-bb20-b197ce3f3d44" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "7260f5f8-39c3-40bd-b1a1-45bfd741cab2", "d8d30e37-0411-4a28-bb20-b197ce3f3d44" });

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "3d9affc7-a917-4a35-97ad-10f049244077");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "0850d92b-1646-4dee-a398-ccb777cad5d1");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "7260f5f8-39c3-40bd-b1a1-45bfd741cab2");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "d8d30e37-0411-4a28-bb20-b197ce3f3d44");

            migrationBuilder.AlterColumn<double>(
                name: "Rating",
                table: "TextMaterials",
                type: "float",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(3,2)",
                oldPrecision: 3,
                oldScale: 2);

            migrationBuilder.AlterColumn<DateTime>(
                name: "PublishDate",
                table: "TextMaterials",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "date");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "d87740fa-a3c4-4e75-9d06-ef555f6712ec", "ab50ed0e-5ac0-49d7-ad97-1cc1013848fd", "Admin", "ADMIN" },
                    { "dee13ae5-06f6-4eca-9a6b-f18890b8ee92", "944e0dc7-ac70-4a88-bc06-d322fcc289a9", "User", "USER" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { "475d6969-511e-4169-9ee4-4d2acc1de191", 0, "e057eba7-2a40-45b0-a7bd-7dccbf79f1b3", "admin@gmail.com", false, false, null, "ADMIN@GMAIL.COM", "ADMIN", "AQAAAAEAACcQAAAAEE7nWckGDJonf0flEApxor6hh9nv17pr/7c3D26l+lGsnLf9OQFYSB/vS6+7CvSgLg==", null, false, "51501854-a3ca-43bb-b237-9b8e3a0ba378", false, "Admin" },
                    { "19d7c79c-374d-44c0-9359-5ffa8840d551", 0, "3ddbc2c8-7bcf-4ece-84f7-8b9547e50ddb", "deleted@gmail.com", false, false, null, "DELETED@GMAIL.COM", "DELETED USER", "AQAAAAEAACcQAAAAEEax9ZRgWoT6rPjBNz6DYKAXv0WapugremJdEhNQ1gNBAWF7rNbd+cMTm/EVUXI/FQ==", null, false, "2c4c3950-93ad-4e21-9538-5122cdb7a7a0", false, "Deleted User" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[,]
                {
                    { "d87740fa-a3c4-4e75-9d06-ef555f6712ec", "475d6969-511e-4169-9ee4-4d2acc1de191" },
                    { "dee13ae5-06f6-4eca-9a6b-f18890b8ee92", "475d6969-511e-4169-9ee4-4d2acc1de191" }
                });

            migrationBuilder.UpdateData(
                table: "TextMaterials",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "PublisherId", "Rating" },
                values: new object[] { "475d6969-511e-4169-9ee4-4d2acc1de191", 0.0 });

            migrationBuilder.UpdateData(
                table: "TextMaterials",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "PublisherId", "Rating" },
                values: new object[] { "475d6969-511e-4169-9ee4-4d2acc1de191", 0.0 });

            migrationBuilder.UpdateData(
                table: "TextMaterials",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "PublisherId", "Rating" },
                values: new object[] { "475d6969-511e-4169-9ee4-4d2acc1de191", 0.0 });
        }
    }
}
