﻿// <auto-generated />
using System;
using Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Data.Migrations
{
    [DbContext(typeof(CardFileContext))]
    [Migration("20201118191957_UniqueConstraintsAdded")]
    partial class UniqueConstraintsAdded
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .UseIdentityColumns()
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.0");

            modelBuilder.Entity("Data.DataEntities.Author", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("Surname")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("Id");

                    b.HasIndex("Name", "Surname")
                        .IsUnique();

                    b.ToTable("Authors");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Name = "Richard",
                            Surname = "White"
                        },
                        new
                        {
                            Id = 2,
                            Name = "Oleg",
                            Surname = "Zigulia"
                        },
                        new
                        {
                            Id = 3,
                            Name = "Olga",
                            Surname = "Kustova"
                        },
                        new
                        {
                            Id = 4,
                            Name = "Nastia",
                            Surname = "Ponomarenko"
                        },
                        new
                        {
                            Id = 5,
                            Name = "Mike",
                            Surname = "Black"
                        });
                });

            modelBuilder.Entity("Data.DataEntities.AuthorTextMaterial", b =>
                {
                    b.Property<int>("AuthorId")
                        .HasColumnType("int");

                    b.Property<int>("TextMaterialId")
                        .HasColumnType("int");

                    b.HasKey("AuthorId", "TextMaterialId");

                    b.HasIndex("TextMaterialId");

                    b.ToTable("AuthorTextMaterials");
                });

            modelBuilder.Entity("Data.DataEntities.Genre", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("Id");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.ToTable("Genres");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Name = "Detective"
                        },
                        new
                        {
                            Id = 2,
                            Name = "Horror"
                        },
                        new
                        {
                            Id = 3,
                            Name = "Comedy"
                        },
                        new
                        {
                            Id = 4,
                            Name = "Fantasy"
                        },
                        new
                        {
                            Id = 5,
                            Name = "Adventure"
                        },
                        new
                        {
                            Id = 6,
                            Name = "Romance"
                        });
                });

            modelBuilder.Entity("Data.DataEntities.GenreTextMaterial", b =>
                {
                    b.Property<int>("GenreId")
                        .HasColumnType("int");

                    b.Property<int>("TextMaterialId")
                        .HasColumnType("int");

                    b.HasKey("GenreId", "TextMaterialId");

                    b.HasIndex("TextMaterialId");

                    b.ToTable("GenreTextMaterials");
                });

            modelBuilder.Entity("Data.DataEntities.TextMaterial", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<string>("Body")
                        .IsRequired()
                        .HasMaxLength(1000)
                        .HasColumnType("nvarchar(1000)");

                    b.Property<DateTime>("PublishDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("PublisherId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<double>("Rating")
                        .HasColumnType("float");

                    b.Property<int>("RatingGradeCount")
                        .HasColumnType("int");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("Body")
                        .IsUnique();

                    b.HasIndex("PublisherId");

                    b.ToTable("TextMaterials");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Body = "Christina visited Miami during her winter vacation. She is from Boston, where it is cold during the winter months. Miami, however, has a very warm climate. There are many sunny days in Miami, and people can go to the beach all year long. Christina spent a good portion of her trip on the beach to relax and sunbathe. However, she also explored Miami and its surroundings.Inspired by Miami’s proximity to the ocean,Christina visited the Miami Seaquarium to learn about marine life.There,she watched a show using trained dolphins, killer whales, and other aquatic mammals. She took a lot of pictures of the sea creatures jumping out of the water and performing tricks.",
                            PublishDate = new DateTime(2011, 12, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            PublisherId = "626b79c9-b1f2-4bf2-ab50-b91dd01f98fb",
                            Rating = 0.0,
                            RatingGradeCount = 0,
                            Title = "Miami"
                        });
                });

            modelBuilder.Entity("Data.DataEntities.User", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("AccessFailedCount")
                        .HasColumnType("int");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Email")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.Property<bool>("EmailConfirmed")
                        .HasColumnType("bit");

                    b.Property<bool>("LockoutEnabled")
                        .HasColumnType("bit");

                    b.Property<DateTimeOffset?>("LockoutEnd")
                        .HasColumnType("datetimeoffset");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.Property<string>("PasswordHash")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PhoneNumber")
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("PhoneNumberConfirmed")
                        .HasColumnType("bit");

                    b.Property<string>("SecurityStamp")
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("TwoFactorEnabled")
                        .HasColumnType("bit");

                    b.Property<string>("UserName")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.HasKey("Id");

                    b.HasIndex("Email")
                        .IsUnique()
                        .HasFilter("[Email] IS NOT NULL");

                    b.HasIndex("NormalizedEmail")
                        .HasDatabaseName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasDatabaseName("UserNameIndex")
                        .HasFilter("[NormalizedUserName] IS NOT NULL");

                    b.HasIndex("UserName")
                        .IsUnique()
                        .HasFilter("[UserName] IS NOT NULL");

                    b.ToTable("AspNetUsers");

                    b.HasData(
                        new
                        {
                            Id = "626b79c9-b1f2-4bf2-ab50-b91dd01f98fb",
                            AccessFailedCount = 0,
                            ConcurrencyStamp = "b935b5b3-e5b0-4921-a2f2-51f48040f710",
                            Email = "admin@gmail.com",
                            EmailConfirmed = false,
                            LockoutEnabled = false,
                            NormalizedEmail = "ADMIN@GMAIL.COM",
                            NormalizedUserName = "ADMIN",
                            PasswordHash = "AQAAAAEAACcQAAAAEO+mY3dG1FjsqU0e2EvHSCyXJ0uaKYRSLAKHUjPBwaAy3aWjtDbUMrW8scAx5jYVVA==",
                            PhoneNumberConfirmed = false,
                            SecurityStamp = "23920b95-0fd3-41fc-9f33-07f2263fc871",
                            TwoFactorEnabled = false,
                            UserName = "Admin"
                        },
                        new
                        {
                            Id = "e52f5d0b-f71a-4457-a9c7-f04bfc7cd730",
                            AccessFailedCount = 0,
                            ConcurrencyStamp = "87599ff4-5193-47e9-9a0a-5527bcf49910",
                            Email = "deleted@gmail.com",
                            EmailConfirmed = false,
                            LockoutEnabled = false,
                            NormalizedEmail = "DELETED@GMAIL.COM",
                            NormalizedUserName = "DELETED USER",
                            PasswordHash = "AQAAAAEAACcQAAAAEClsc93P/MRpwiPEQG3NRXctLL6VuiUaOt37LjayFx6hvBZmtAN8GEkDZCGXdh1AXw==",
                            PhoneNumberConfirmed = false,
                            SecurityStamp = "041ad3d6-0a77-4562-875d-2f1c5053cb58",
                            TwoFactorEnabled = false,
                            UserName = "Deleted User"
                        });
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRole", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasDatabaseName("RoleNameIndex")
                        .HasFilter("[NormalizedName] IS NOT NULL");

                    b.ToTable("AspNetRoles");

                    b.HasData(
                        new
                        {
                            Id = "b7bbad19-1687-4233-9372-04460b911878",
                            ConcurrencyStamp = "2336d1b6-a085-48e6-860b-7e0b13691510",
                            Name = "Admin",
                            NormalizedName = "ADMIN"
                        },
                        new
                        {
                            Id = "0aff50d8-2cad-40da-8db3-493f44af5908",
                            ConcurrencyStamp = "b07dfec1-ed29-4b57-97f8-a4b877995660",
                            Name = "User",
                            NormalizedName = "USER"
                        });
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<string>("ClaimType")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ClaimValue")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("RoleId")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<string>("ClaimType")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ClaimValue")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("UserId")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("ProviderKey")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("ProviderDisplayName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("UserId")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("RoleId")
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");

                    b.HasData(
                        new
                        {
                            UserId = "626b79c9-b1f2-4bf2-ab50-b91dd01f98fb",
                            RoleId = "b7bbad19-1687-4233-9372-04460b911878"
                        },
                        new
                        {
                            UserId = "626b79c9-b1f2-4bf2-ab50-b91dd01f98fb",
                            RoleId = "0aff50d8-2cad-40da-8db3-493f44af5908"
                        });
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("LoginProvider")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("Value")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("Data.DataEntities.AuthorTextMaterial", b =>
                {
                    b.HasOne("Data.DataEntities.Author", "Author")
                        .WithMany("AuthorTextMaterials")
                        .HasForeignKey("AuthorId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Data.DataEntities.TextMaterial", "TextMaterial")
                        .WithMany("AuthorTextMaterials")
                        .HasForeignKey("TextMaterialId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Author");

                    b.Navigation("TextMaterial");
                });

            modelBuilder.Entity("Data.DataEntities.GenreTextMaterial", b =>
                {
                    b.HasOne("Data.DataEntities.Genre", "Genre")
                        .WithMany("GenreTextMaterials")
                        .HasForeignKey("GenreId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Data.DataEntities.TextMaterial", "TextMaterial")
                        .WithMany("GenreTextMaterials")
                        .HasForeignKey("TextMaterialId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Genre");

                    b.Navigation("TextMaterial");
                });

            modelBuilder.Entity("Data.DataEntities.TextMaterial", b =>
                {
                    b.HasOne("Data.DataEntities.User", "Publisher")
                        .WithMany("Texts")
                        .HasForeignKey("PublisherId");

                    b.Navigation("Publisher");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole", null)
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("Data.DataEntities.User", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("Data.DataEntities.User", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole", null)
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Data.DataEntities.User", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.HasOne("Data.DataEntities.User", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Data.DataEntities.Author", b =>
                {
                    b.Navigation("AuthorTextMaterials");
                });

            modelBuilder.Entity("Data.DataEntities.Genre", b =>
                {
                    b.Navigation("GenreTextMaterials");
                });

            modelBuilder.Entity("Data.DataEntities.TextMaterial", b =>
                {
                    b.Navigation("AuthorTextMaterials");

                    b.Navigation("GenreTextMaterials");
                });

            modelBuilder.Entity("Data.DataEntities.User", b =>
                {
                    b.Navigation("Texts");
                });
#pragma warning restore 612, 618
        }
    }
}
