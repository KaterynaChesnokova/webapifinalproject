﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class UniqueConstraintsAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "8feb5f6d-4c69-4946-a2f2-c42b8f0077df", "3830e099-6e8e-4341-b1b8-8136ebce5057" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "e82baad0-55b1-49e6-9a76-8e27fc33da10", "3830e099-6e8e-4341-b1b8-8136ebce5057" });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "8feb5f6d-4c69-4946-a2f2-c42b8f0077df");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "e82baad0-55b1-49e6-9a76-8e27fc33da10");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "3830e099-6e8e-4341-b1b8-8136ebce5057");

            migrationBuilder.AlterColumn<string>(
                name: "Body",
                table: "TextMaterials",
                type: "nvarchar(1000)",
                maxLength: 1000,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Genres",
                type: "nvarchar(450)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "Surname",
                table: "Authors",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Authors",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "b7bbad19-1687-4233-9372-04460b911878", "2336d1b6-a085-48e6-860b-7e0b13691510", "Admin", "ADMIN" },
                    { "0aff50d8-2cad-40da-8db3-493f44af5908", "b07dfec1-ed29-4b57-97f8-a4b877995660", "User", "USER" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { "626b79c9-b1f2-4bf2-ab50-b91dd01f98fb", 0, "b935b5b3-e5b0-4921-a2f2-51f48040f710", "admin@gmail.com", false, false, null, "ADMIN@GMAIL.COM", "ADMIN", "AQAAAAEAACcQAAAAEO+mY3dG1FjsqU0e2EvHSCyXJ0uaKYRSLAKHUjPBwaAy3aWjtDbUMrW8scAx5jYVVA==", null, false, "23920b95-0fd3-41fc-9f33-07f2263fc871", false, "Admin" },
                    { "e52f5d0b-f71a-4457-a9c7-f04bfc7cd730", 0, "87599ff4-5193-47e9-9a0a-5527bcf49910", "deleted@gmail.com", false, false, null, "DELETED@GMAIL.COM", "DELETED USER", "AQAAAAEAACcQAAAAEClsc93P/MRpwiPEQG3NRXctLL6VuiUaOt37LjayFx6hvBZmtAN8GEkDZCGXdh1AXw==", null, false, "041ad3d6-0a77-4562-875d-2f1c5053cb58", false, "Deleted User" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "b7bbad19-1687-4233-9372-04460b911878", "626b79c9-b1f2-4bf2-ab50-b91dd01f98fb" });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "0aff50d8-2cad-40da-8db3-493f44af5908", "626b79c9-b1f2-4bf2-ab50-b91dd01f98fb" });

            migrationBuilder.UpdateData(
                table: "TextMaterials",
                keyColumn: "Id",
                keyValue: 1,
                column: "PublisherId",
                value: "626b79c9-b1f2-4bf2-ab50-b91dd01f98fb");

            migrationBuilder.CreateIndex(
                name: "IX_TextMaterials_Body",
                table: "TextMaterials",
                column: "Body",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Genres_Name",
                table: "Genres",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Authors_Name_Surname",
                table: "Authors",
                columns: new[] { "Name", "Surname" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_Email",
                table: "AspNetUsers",
                column: "Email",
                unique: true,
                filter: "[Email] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_UserName",
                table: "AspNetUsers",
                column: "UserName",
                unique: true,
                filter: "[UserName] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_TextMaterials_Body",
                table: "TextMaterials");

            migrationBuilder.DropIndex(
                name: "IX_Genres_Name",
                table: "Genres");

            migrationBuilder.DropIndex(
                name: "IX_Authors_Name_Surname",
                table: "Authors");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_Email",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_UserName",
                table: "AspNetUsers");

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "0aff50d8-2cad-40da-8db3-493f44af5908", "626b79c9-b1f2-4bf2-ab50-b91dd01f98fb" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "b7bbad19-1687-4233-9372-04460b911878", "626b79c9-b1f2-4bf2-ab50-b91dd01f98fb" });

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "e52f5d0b-f71a-4457-a9c7-f04bfc7cd730");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "0aff50d8-2cad-40da-8db3-493f44af5908");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b7bbad19-1687-4233-9372-04460b911878");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "626b79c9-b1f2-4bf2-ab50-b91dd01f98fb");

            migrationBuilder.AlterColumn<string>(
                name: "Body",
                table: "TextMaterials",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(1000)",
                oldMaxLength: 1000);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Genres",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.AlterColumn<string>(
                name: "Surname",
                table: "Authors",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Authors",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "e82baad0-55b1-49e6-9a76-8e27fc33da10", "d10e0e28-dbb4-4ec7-a539-490dd17e491c", "Admin", "ADMIN" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "8feb5f6d-4c69-4946-a2f2-c42b8f0077df", "fd89c6a0-3da1-43ef-8fa5-390450fa8ca9", "User", "USER" });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { "3830e099-6e8e-4341-b1b8-8136ebce5057", 0, "8125e5fe-6276-48bd-8d32-9d38407f978e", "admin@gmail.com", false, false, null, "ADMIN@GMAIL.COM", "ADMIN@GMAIL.COM", "AQAAAAEAACcQAAAAEEhQ8XaCYWOobtVp4v2ocaZRA509EOdxKL1113VANOcWgtyWNgZuMi8TpqhIS1Jtyw==", null, false, "d708940b-ed97-45bb-9b73-2883e8aff2e4", false, "admin@gmail.com" });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "e82baad0-55b1-49e6-9a76-8e27fc33da10", "3830e099-6e8e-4341-b1b8-8136ebce5057" });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "8feb5f6d-4c69-4946-a2f2-c42b8f0077df", "3830e099-6e8e-4341-b1b8-8136ebce5057" });

            migrationBuilder.UpdateData(
                table: "TextMaterials",
                keyColumn: "Id",
                keyValue: 1,
                column: "PublisherId",
                value: "3830e099-6e8e-4341-b1b8-8136ebce5057");
        }
    }
}
