﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class Seed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "9e788238-6714-401f-8235-7831d535bfed", "e158444a-332f-4f37-b5c1-5484421ed196", "Admin", "ADMIN" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "dee72389-2ec8-429b-8dc2-10536a8af568", "b2f31431-4c25-4307-9168-356badf96cf3", "User", "USER" });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { "2f87a355-dc64-4777-9bb3-71ecb72ee89d", 0, "7506f68b-390e-4db1-b3bd-4377b7d2de75", "admin@gmail.com", false, false, null, "ADMIN@GMAIL.COM", "SUPERADMIN", "AQAAAAEAACcQAAAAEKZECIbdxHU4kA9Zs1XFM986LiCF4xt4TK26tgb33op9b6trEISns99hQ/jWinmsoQ==", null, false, "f2dee034-0042-4ed4-9a6e-f211dadfeb00", false, "SuperAdmin" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "9e788238-6714-401f-8235-7831d535bfed");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "dee72389-2ec8-429b-8dc2-10536a8af568");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2f87a355-dc64-4777-9bb3-71ecb72ee89d");
        }
    }
}
