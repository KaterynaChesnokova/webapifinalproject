﻿using Data.DataEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data
{
    /// <summary>
    /// class for database initialization
    /// </summary>
    public static class CardFileInitializer
    {
        /// <summary>
        /// database initialization method
        /// </summary>
        public static void Seed()
        {
            using (CardFileContext context = new CardFileContext())
            {
                context.AuthorTextMaterials.Add(new AuthorTextMaterial
                { Author = context.Authors.Find(1), TextMaterial = context.TextMaterials.Find(1) });
                context.AuthorTextMaterials.Add(new AuthorTextMaterial
                { Author = context.Authors.Find(2), TextMaterial = context.TextMaterials.Find(1) });

                context.AuthorTextMaterials.Add(new AuthorTextMaterial
                { Author = context.Authors.Find(4), TextMaterial = context.TextMaterials.Find(2) });
                context.AuthorTextMaterials.Add(new AuthorTextMaterial
                { Author = context.Authors.Find(6), TextMaterial = context.TextMaterials.Find(2) });

                context.AuthorTextMaterials.Add(new AuthorTextMaterial
                { Author = context.Authors.Find(7), TextMaterial = context.TextMaterials.Find(3) });


                context.GenreTextMaterials.Add(new GenreTextMaterial
                { Genre = context.Genres.Find(1), TextMaterial = context.TextMaterials.Find(1) });
                context.GenreTextMaterials.Add(new GenreTextMaterial
                { Genre = context.Genres.Find(2), TextMaterial = context.TextMaterials.Find(1) });

                context.GenreTextMaterials.Add(new GenreTextMaterial
                { Genre = context.Genres.Find(9), TextMaterial = context.TextMaterials.Find(2) });
                context.GenreTextMaterials.Add(new GenreTextMaterial
                { Genre = context.Genres.Find(5), TextMaterial = context.TextMaterials.Find(2) });

                context.GenreTextMaterials.Add(new GenreTextMaterial
                { Genre = context.Genres.Find(8), TextMaterial = context.TextMaterials.Find(3) });
                context.SaveChanges();
            }
        }
    }
}
