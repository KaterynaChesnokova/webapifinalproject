﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Data.DataEntities
{
    /// <summary>
    /// class represents text
    /// </summary>
    public class TextMaterial : BaseEntity
    {
        /// <summary>
        /// text title
        /// </summary>
        [Required]
        public string Title { get; set; }

        /// <summary>
        /// text body
        /// </summary>
        [Required]
        [StringLength(1000, MinimumLength = 50)]
        public string Body { get; set; }

        /// <summary>
        /// date when text was added
        /// </summary>
        [Required]
        public DateTime PublishDate { get; set; }

        /// <summary>
        /// text rating
        /// </summary>
        [Required]
        public decimal Rating { get; set; }

        /// <summary>
        /// number of grades for rating calculation
        /// </summary>
        [Required]
        public int RatingGradeCount { get; set; }

        /// <summary>
        /// id of user who published text
        /// </summary>
        public string PublisherId { get; set; }
        public User Publisher { get; set; }

        public List<AuthorTextMaterial> AuthorTextMaterials { get; set; }
        public List<GenreTextMaterial> GenreTextMaterials { get; set; }
    }
}
