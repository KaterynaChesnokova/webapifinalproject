﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Data.DataEntities
{
    public class GenreTextMaterial
    {
        public int GenreId { get; set; }
        [ForeignKey("GenreId")]
        public virtual Genre Genre { get; set; }


        public int TextMaterialId { get; set; }
        [ForeignKey("TextMaterialId")]
        public virtual TextMaterial TextMaterial { get; set; }
    }
}
