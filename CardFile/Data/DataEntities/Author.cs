﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Data.DataEntities
{
    public class Author : BaseEntity
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Surname { get; set; }

        public List<AuthorTextMaterial> AuthorTextMaterials { get; set; }
    }
}
