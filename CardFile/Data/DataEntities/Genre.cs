﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Data.DataEntities
{
    public class Genre : BaseEntity
    {
        [Required]
        public string Name { get; set; }

        public List<GenreTextMaterial> GenreTextMaterials { get; set; }
    }
}
