﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.DataEntities
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}
