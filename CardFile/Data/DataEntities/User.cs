﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.DataEntities
{
    /// <summary>
    /// class represents user
    /// </summary>
    public class User : IdentityUser
    {
        /// <summary>
        /// collection of texts published by user 
        /// </summary>
        public virtual ICollection<TextMaterial> Texts { get; set; }
    }
}
