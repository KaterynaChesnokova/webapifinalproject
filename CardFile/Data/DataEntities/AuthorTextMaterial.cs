﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Data.DataEntities
{
    public class AuthorTextMaterial
    {

        public int AuthorId { get; set; }
        [ForeignKey("AuthorId")]
        public virtual Author Author { get; set; }


        public int TextMaterialId { get; set; }
        [ForeignKey("TextMaterialId")]
        public virtual TextMaterial TextMaterial { get; set; }
    }
}
