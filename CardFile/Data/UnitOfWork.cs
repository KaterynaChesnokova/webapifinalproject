﻿using Data.DataEntities;
using Data.Interfaces;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace Data
{
    /// <summary>
    /// class encapsulates all repositories
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        /// <summary>
        /// database context
        /// </summary>
        readonly CardFileContext db;

        public UnitOfWork(CardFileContext _db, IAuthorRepository _authorRepository, IGenreRepository _genreRepository,
            ITextMaterialRepository _textMaterialRepository, IUserRepository _userRepository)
        {
            db = _db;
            AuthorRepository = _authorRepository;
            GenreRepository = _genreRepository;
            TextMaterialRepository = _textMaterialRepository;
            UserRepository = _userRepository;
        }

        public IAuthorRepository AuthorRepository { get; }

        public IGenreRepository GenreRepository { get; }

        public ITextMaterialRepository TextMaterialRepository { get; }

        public IUserRepository UserRepository { get; }

        /// <summary>
        /// calls save method of database context for all repositories
        /// </summary>
        public async Task<int> SaveAsync()
        {
            return await db.SaveChangesAsync();
        }
    }
}
