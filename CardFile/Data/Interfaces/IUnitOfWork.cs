﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    public interface IUnitOfWork
    {
        IAuthorRepository AuthorRepository{ get; }

        IGenreRepository GenreRepository{ get; }

        ITextMaterialRepository TextMaterialRepository { get; }

        IUserRepository UserRepository { get; }

        Task<int> SaveAsync();
    }

}
