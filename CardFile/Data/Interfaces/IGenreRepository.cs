﻿using Data.DataEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    public interface IGenreRepository : IRepository<Genre>
    {
        IQueryable<Genre> GetAllWithDetails();

        Task<Genre> GetByIdWithDetailsAsync(int id);
    }
}
