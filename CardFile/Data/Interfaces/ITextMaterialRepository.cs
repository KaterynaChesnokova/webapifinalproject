﻿using Data.DataEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    public interface ITextMaterialRepository : IRepository<TextMaterial>
    {
        IQueryable<TextMaterial> GetAllWithDetails();

        Task<TextMaterial> GetByIdWithDetailsAsync(int id);
    }
}
