﻿using Data.DataEntities;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    public interface IUserRepository
    {
        Task<bool> UserCheckPasswordAsync(User user, string password);
        Task<IdentityResult> UserCreateAsync(User user, string password);
        Task<User> UserFindByNameAsync(string name);
        Task<User> UserFindByEmailAsync(string email);
        Task<IList<string>> UserGetRolesAsync(User user);
        Task<bool> RoleExistsAsync(string roleName);
        Task<IdentityResult> CreateAsync(IdentityRole role);
        Task<IdentityResult> AddToRoleAsync(User user, string role);
        Task<IdentityResult> CreateAndAddToRole(User user, string password, string role);
        Task<IdentityResult> UserDeleteAccountAsync(string userName);
        Task<User> UserFindByIdAsync(string id);
    }
}