﻿using Data.DataEntities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data
{
    public class CardFileContext : IdentityDbContext<User>
    {
        public CardFileContext()
        {
        }

        public CardFileContext(DbContextOptions<CardFileContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AuthorTextMaterial>()
                    .HasKey(s => new { s.AuthorId, s.TextMaterialId });

            modelBuilder.Entity<GenreTextMaterial>()
                                .HasKey(s => new { s.GenreId, s.TextMaterialId });

            modelBuilder.Entity<Genre>()
                .HasIndex(u => u.Name)
                .IsUnique();

            modelBuilder.Entity<Author>()
                .HasIndex(u => new { u.Name, u.Surname })
                .IsUnique();

            modelBuilder.Entity<TextMaterial>()
                .HasIndex(u => u.Body)
                .IsUnique();

            modelBuilder.Entity<TextMaterial>().Property(p => p.Rating).HasPrecision(3, 2);
            modelBuilder.Entity<TextMaterial>().Property(p => p.PublishDate).HasColumnType("date");

            modelBuilder.Entity<User>()
               .HasIndex(u => u.Email)
               .IsUnique();

            modelBuilder.Entity<User>()
               .HasIndex(u => u.UserName)
               .IsUnique();

            modelBuilder
                .Entity<User>()
                .Property(e => e.Id)
                .ValueGeneratedOnAdd();

            var role1 = new IdentityRole { Id = Guid.NewGuid().ToString(), Name = "Admin", NormalizedName = "ADMIN" };
            var role2 = new IdentityRole { Id = Guid.NewGuid().ToString(), Name = "User", NormalizedName = "USER" };

            modelBuilder.Entity<IdentityRole>().HasData(role1, role2);

            var admin = new User
            {
                Id = Guid.NewGuid().ToString(),
                Email = "admin@gmail.com",
                NormalizedEmail = "ADMIN@GMAIL.COM",
                UserName = "Admin",
                NormalizedUserName = "ADMIN"
            };

            var deletedUser = new User
            {
                Id = Guid.NewGuid().ToString(),
                Email = "deleted@gmail.com",
                NormalizedEmail = "DELETED@GMAIL.COM",
                UserName = "Deleted User",
                NormalizedUserName = "DELETED USER"
            };

            var hasher = new PasswordHasher<User>();
            var adminPassword = hasher.HashPassword(admin, "Admin1!");
            var deletedUserPassword = hasher.HashPassword(deletedUser, "DeletedUser1!");

            admin.PasswordHash = adminPassword;
            deletedUser.PasswordHash = deletedUserPassword;

            modelBuilder.Entity<IdentityUserRole<string>>().HasData(
                new IdentityUserRole<string> { RoleId = role1.Id, UserId = admin.Id },
                new IdentityUserRole<string> { RoleId = role2.Id, UserId = admin.Id });


            modelBuilder.Entity<User>().HasData(admin, deletedUser);

            var genres = new[]
             {
               new Genre { Id = 1, Name = "Detective" },
               new Genre { Id = 2, Name = "Horror" },
               new Genre { Id = 3, Name = "Comedy" },
               new Genre { Id = 4, Name = "Fantasy" },
               new Genre { Id = 5, Name = "Adventure" },
               new Genre { Id = 6, Name = "Romance" },
               new Genre { Id = 7, Name = "Westerns" },
               new Genre { Id = 8, Name = "History" },
               new Genre { Id = 9, Name = "Children’s" },
            };

            modelBuilder.Entity<Genre>().HasData(genres);

            var authors = new[]
            {
                new Author { Id = 1, Name = "Richard", Surname = "White" },
                new Author { Id = 2, Name = "Oleg", Surname = "Zigulia" },
                new Author { Id = 3, Name = "Olga", Surname = "Kustova" },
                new Author { Id = 4, Name = "Nastia", Surname = "Ponomarenko" },
                new Author { Id = 5, Name = "Mike", Surname = "Black" },
                new Author { Id = 6, Name = "Linda", Surname = "Kornel" },
                new Author { Id = 7, Name = "Maria", Surname = "Mirabella" }
            };

            modelBuilder.Entity<Author>().HasData(authors);

            var texts = new[]
            {
                new TextMaterial
                            {
                                Id = 1,
                                Title = "Miami",
                                Body = "Christina visited Miami during her winter vacation." +
                                    " She is from Boston, where it is cold during the winter months. Miami, however, has a very warm climate." +
                                    " There are many sunny days in Miami, and people can go to the beach all year long. Christina spent a good" +
                                    " portion of her trip on the beach to relax and sunbathe. However, she also explored Miami and its surroundings." +
                                    "Inspired by Miami’s proximity to the ocean,Christina visited the Miami Seaquarium to learn about marine life.There," +
                                    "she watched a show using trained dolphins, killer whales, and other aquatic mammals. She took a lot of pictures of the " +
                                    "sea creatures jumping out of the water and performing tricks.",
                                PublishDate = new DateTime(2011, 12, 1),
                                PublisherId = admin.Id,
                                Rating = 0,
                                RatingGradeCount = 0
                            },
                new TextMaterial
                            {
                                Id = 2,
                                Title = "Halloween",
                                Body = "Halloween (also referred to as All Hollows' Eve) is a holiday that's celebrated in America on 31 October of each year," +
                                " regardless of what day of the week this date falls on. Although it is rooted in religion, Halloween today is enjoyed mainly " +
                                "because of its decorations, costumes, candy, treats, and general excitement, and furthermore, it is enjoyed by most everyone."+
                                "Before Halloween, many individuals carve a design into an orange-colored pumpkin, or a solid, durable vegetable. Once a personally " +
                                "satisfying design is carved, a lit candle is typically put inside a pumpkin, thereby making it a Jack-O-Lantern. At night, this design " +
                                "lights up against the darkness.",
                                PublishDate = new DateTime(2020, 9, 10),
                                PublisherId = admin.Id,
                                Rating = 0,
                                RatingGradeCount = 0
                            },
                new TextMaterial
                            {
                                Id = 3,
                                Title = "London",
                                Body = "London is a famous and historic city. It is the capital of England in the United Kingdom. The city is quite popular for " +
                                "international tourism because London is home to one of the oldest-standing monarchies in the western hemisphere. Rita and Joanne " +
                                "recently traveled to London. They were very excited for their trip because this was their first journey overseas from the United States."+
                                "Among the popular sights that Rita and Joanne visited are Big Ben, Buckingham Palace, and the London Eye. Big Ben is one of London’s most " +
                                "famous monuments. It is a large clock tower located at the northern end of Westminster Palace. The clock tower is 96 meters tall.",
                                PublishDate = new DateTime(2001, 7,20),
                                PublisherId = admin.Id,
                                Rating = 0,
                                RatingGradeCount = 0
                            }
        };


            modelBuilder.Entity<TextMaterial>().HasData(texts);

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<TextMaterial> TextMaterials { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<GenreTextMaterial> GenreTextMaterials { get; set; }
        public DbSet<AuthorTextMaterial> AuthorTextMaterials { get; set; }
    }
}
