﻿using Data.DataEntities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class TextMaterialRepository : ITextMaterialRepository
    {
        readonly CardFileContext db;

        public TextMaterialRepository(CardFileContext _db)
        {
            db = _db;
        }

        public async Task AddAsync(TextMaterial entity)
        {
            await db.TextMaterials.AddAsync(entity);
        }

        public void Delete(TextMaterial entity)
        {
            db.TextMaterials.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            var entity = await db.TextMaterials.FirstOrDefaultAsync(x => x.Id == id);
            db.TextMaterials.Remove(entity);
        }

        public IQueryable<TextMaterial> FindAll()
        {
            return db.TextMaterials;
        }

        public IQueryable<TextMaterial> GetAllWithDetails()
        {
            return db.TextMaterials.Include(x => x.Publisher).ThenInclude(x => x.Texts)
               .Include(y => y.AuthorTextMaterials).ThenInclude(y => y.Author)
               .Include(z => z.GenreTextMaterials).ThenInclude(z => z.Genre);
        }

        public async Task<TextMaterial> GetByIdAsync(int id)
        {
            return await db.TextMaterials.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<TextMaterial> GetByIdWithDetailsAsync(int id)
        {
            return await GetAllWithDetails().FirstOrDefaultAsync(x => x.Id == id);
        }

        public void Update(TextMaterial entity)
        {
            db.Entry(entity).State = EntityState.Modified;
        }
    }
}
