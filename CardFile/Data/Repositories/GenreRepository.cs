﻿using Data.DataEntities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class GenreRepository : IGenreRepository
    {
        readonly CardFileContext db;

        public GenreRepository(CardFileContext _db)
        {
            db = _db;
        }

        public async Task AddAsync(Genre entity)
        {
            await db.Genres.AddAsync(entity);
        }

        public void Delete(Genre entity)
        {
            db.Genres.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            var entity = await db.Genres.FindAsync(id);
            db.Genres.Remove(entity);
        }

        public IQueryable<Genre> FindAll()
        {
            return db.Genres;
        }

        public IQueryable<Genre> GetAllWithDetails()
        {
            return db.Genres.Include(x => x.GenreTextMaterials).ThenInclude(x => x.Genre)
                .Include(x => x.GenreTextMaterials).ThenInclude(x => x.TextMaterial);
        }

        public async Task<Genre> GetByIdAsync(int id)
        {
            return await db.Genres.FindAsync(id);
        }

        public async Task<Genre> GetByIdWithDetailsAsync(int id)
        {
            return await GetAllWithDetails().FirstOrDefaultAsync(x => x.Id == id);
        }

        public void Update(Genre entity)
        {
            db.Entry(entity).State = EntityState.Modified;
        }
    }
}
