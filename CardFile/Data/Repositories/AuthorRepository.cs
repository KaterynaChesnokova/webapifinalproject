﻿using Data.DataEntities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class AuthorRepository : IAuthorRepository
    {
        readonly CardFileContext db;

        public AuthorRepository(CardFileContext _db)
        {
            db = _db;
        }

        public async Task AddAsync(Author entity)
        {
            await db.Authors.AddAsync(entity);
        }

        public void Delete(Author entity)
        {
            db.Authors.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            var entity = await db.Authors.FindAsync(id);
            db.Authors.Remove(entity);
        }

        public IQueryable<Author> FindAll()
        {
            return db.Authors;
        }

        public IQueryable<Author> GetAllWithDetails()
        {
            return db.Authors.Include(x => x.AuthorTextMaterials).ThenInclude(x => x.TextMaterial);
        }

        public async Task<Author> GetByIdAsync(int id)
        {
            return await db.Authors.FindAsync(id);
        }

        public async Task<Author> GetByIdWithDetailsAsync(int id)
        {
            return await GetAllWithDetails().FirstOrDefaultAsync(x => x.Id == id);
        }

        public void Update(Author entity)
        {
            db.Entry(entity).State = EntityState.Modified;
        }
    }
}
