﻿using Data.DataEntities;
using Data.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class UserRepository : IUserRepository
    {
        readonly UserManager<User> userManager;
        readonly RoleManager<IdentityRole> roleManager;
        readonly CardFileContext db;

        public UserRepository(CardFileContext _db, UserManager<User> userManager, RoleManager<IdentityRole> roleManager)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
            db = _db;
        }

        public async Task<User> UserFindByIdAsync(string id)
        {
            return await db.Users.Include(x => x.Texts).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<User> UserFindByNameAsync(string name)
        {
            return await db.Users.Include(x => x.Texts).FirstOrDefaultAsync(x => x.UserName == name);
        }

        public async Task<User> UserFindByEmailAsync(string email)
        {
            return await db.Users.Include(x => x.Texts).FirstOrDefaultAsync(x => x.Email == email);
        }

        public async Task<bool> UserCheckPasswordAsync(User user, string password)
        {
            return await userManager.CheckPasswordAsync(user, password);
        }

        public async Task<IList<string>> UserGetRolesAsync(User user)
        {
            return await userManager.GetRolesAsync(user);
        }

        public async Task<IdentityResult> UserCreateAsync(User user, string password)
        {
            return await userManager.CreateAsync(user, password);
        }

        public async Task<bool> RoleExistsAsync(string roleName)
        {
            return await roleManager.RoleExistsAsync(roleName);
        }

        public async Task<IdentityResult> CreateAsync(IdentityRole role)
        {
            return await roleManager.CreateAsync(role);
        }

        public async Task<IdentityResult> AddToRoleAsync(User user, string role)
        {
            return await userManager.AddToRoleAsync(user, role);
        }

        public async Task<IdentityResult> CreateAndAddToRole(User user, string password, string role)
        {
            await userManager.CreateAsync(user, password);
            return await userManager.AddToRoleAsync(user, role);
        }

        public async Task<IdentityResult> UserDeleteAccountAsync(string userName)
        {
            var user = await UserFindByNameAsync(userName);
            return await userManager.DeleteAsync(user);
        }
    }
}
