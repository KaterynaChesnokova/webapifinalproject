import { LoggedUser } from './../../interfaces/logged-user';
import { Register } from './../../interfaces/register';
import { Login } from './../../interfaces/login';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  url='https://localhost:44301/api/Authenticate/';
  private currentUserSubject: BehaviorSubject<LoggedUser|null>;
  public currentUser: Observable<LoggedUser|null>;

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<LoggedUser|null>
      (JSON.parse(localStorage.getItem('currentUser')||'{}'));
    this.currentUserSubject.next(null);
    this.currentUser = this.currentUserSubject.asObservable();
   }

   public getCurrentUserValue(): LoggedUser|null{
    return this.currentUserSubject.value;
  }

  login(loginModel:Login){
  return this.http.post(this.url+'login', loginModel)
    .pipe(
      map((user:any) =>{
        localStorage.setItem('currentUser', JSON.stringify(user));
        localStorage.setItem('id_token', user.token);
        this.currentUserSubject.next(user);
        return user;
      }
    ));
  }

  register(registerModel:Register){
    return this.http.post(this.url+'register', registerModel)
    .pipe(
      map((user:any) =>{
        localStorage.setItem('currentUser', JSON.stringify(user));
        localStorage.setItem('id_token', user.token);
        this.currentUserSubject.next(user);
        return user;
      }
    ));
  }

  deleteAccount(){
  }

  public isLoggedIn():boolean {
    return this.getCurrentUserValue()!=null;
  }

  logout(){
    localStorage.removeItem('currentUser');
    localStorage.removeItem('id_token');
    this.currentUserSubject.next(null);
  }
}
