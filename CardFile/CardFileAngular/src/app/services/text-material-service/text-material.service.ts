import { GetTextMaterial } from './../../interfaces/get-text-material';
import { UpdateTextMaterialRating } from './../../interfaces/update-text-material-rating';
import { UpdateTextMaterial } from './../../interfaces/update-text-material';
import { AddTextMaterial } from './../../interfaces/add-text-material';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TextMaterialService {

  url='https://localhost:44301/api/TextMaterials/';

  constructor(private http: HttpClient) { }

  getAllTextMaterials():Observable<GetTextMaterial[]>{
    return this.http.get<GetTextMaterial[]>(this.url);
  }

  getAllTextMatWithGenre(genre:string):Observable<GetTextMaterial[]>{
    return this.http.get<GetTextMaterial[]>(this.url+'withgenre/'+genre);
  }

  getAllTextMatWithAuthor(name:string,surname:string):Observable<GetTextMaterial[]>{
    return this.http.get<GetTextMaterial[]>(this.url+'withauthor/'+name+'/'+surname);
  }

  getTextMatById(id:number):Observable<GetTextMaterial>{
    return this.http.get<GetTextMaterial>(this.url+id);
  }

  addTextMaterial(text:AddTextMaterial):Observable<any>{
    return this.http.post(this.url,text);
  }

  updateTextMaterial(text:UpdateTextMaterial):Observable<any>{
    return this.http.put(this.url,text);
  }

  updateTextMaterialRating(rating:UpdateTextMaterialRating):Observable<any>{
    return this.http.put(this.url+'rate',rating);
  }

  deleteTextMaterial(id:number): Observable<any> {
    return this.http.delete(this.url + id);
  }
}
