import { AddAuthor } from './../../interfaces/add-author';
import { GetAuthor } from './../../interfaces/get-author';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorService {
  url='https://localhost:44301/api/Authors/';

  constructor(private http: HttpClient) { }

  getAllAuthors():Observable<GetAuthor[]>{
    return this.http.get<GetAuthor[]>(this.url);
  }
  
  getAuthorById(id:number):Observable<GetAuthor>{
    return this.http.get<GetAuthor>(this.url+id);
  }

  addAuthor(newAuthor:AddAuthor):Observable<any>{
    return this.http.post(this.url,newAuthor);
  }

  deleteAuthor(id:number):Observable<any>{
    return this.http.delete(this.url+id);
  }
}
