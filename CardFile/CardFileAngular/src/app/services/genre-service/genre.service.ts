import { AddGenre } from './../../interfaces/add-genre';
import { GetGenre } from './../../interfaces/get-genre';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class GenreService {

  url = 'https://localhost:44301/api/Genres/';

  constructor(private http: HttpClient) { }

  getAllGenres():Observable<GetGenre[]>{
    return this.http.get<GetGenre[]>(this.url);
  }

  getGenreById(id:number):Observable<GetGenre>{
    return this.http.get<GetGenre>(this.url+id);
  }

  addGenre(newGenre:AddGenre):Observable<any>{
    return this.http.post(this.url,newGenre);
  }

  deleteGenre(id:number):Observable<any>{
    return this.http.delete(this.url+id);
  }
}
