import { Role } from './../../interfaces/role';
import { map } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth-service/auth.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {

  isAuth = false;
  roles:string[]=[];
  public login! : string;

  constructor(private authService: AuthService,private router: Router) {
    this.authService.currentUser.subscribe( user => {
      if(user==null){
        this.isAuth = false;
        this.roles=[];
        this.login = '';
      }
      else{
        this.isAuth = true;
        this.roles=user.roles;
        this.login = user.userName;
      }
    })
   }

  ngOnInit(): void {
    let currentUser =  this.authService.getCurrentUserValue();
    this.isAuth = currentUser == null ? false : true;
    this.login = currentUser == null ? '' : currentUser.userName;
  }

  logout(){
    this.authService.logout();
    this.router.navigate(['/login']);
  } 
}
