import { AuthGuard } from './../helpers/auth-guard';
import { AddTextMaterialComponent } from './../components/add-text-material/add-text-material.component';
import { AddGenreComponent } from './../components/add-genre/add-genre.component';
import { AddAuthorComponent } from './../components/add-author/add-author.component';
import { TextMaterialWithAuthorComponent } from './../components/text-material-with-author/text-material-with-author.component';
import { TextMaterialWithGenreComponent } from './../components/text-material-with-genre/text-material-with-genre.component';
import { AuthorsComponent } from './../components/authors/authors.component';
import { RegisterComponent } from './../components/register/register.component';
import { LoginComponent } from './../components/login/login.component';

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TextMaterialsComponent } from '../components/text-materials/text-materials.component';
import { GenresComponent } from '../components/genres/genres.component';

const routes: Routes = [
      {path: 'login', component: LoginComponent},
      {path: 'register', component: RegisterComponent},
      { path: 'addauthor', component: AddAuthorComponent, canActivate: [AuthGuard]},
      { path: 'addgenre', component: AddGenreComponent, canActivate: [AuthGuard]},
      { path: 'addtextmaterial', component: AddTextMaterialComponent, canActivate: [AuthGuard]},
      { path: 'textmaterials/withgenre/:genre', component: TextMaterialWithGenreComponent},
      { path: 'textmaterials/withauthor/:name/:surname', component: TextMaterialWithAuthorComponent},
      { path: 'textmaterials', component: TextMaterialsComponent},
      { path: 'authors', component: AuthorsComponent},
      { path: 'genres', component: GenresComponent},
      { path: '', redirectTo: 'textmaterials', pathMatch: 'full' },
      { path: '**', redirectTo: 'textmaterials' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
],
  exports: [RouterModule]
})
export class AppRoutingModule { }
