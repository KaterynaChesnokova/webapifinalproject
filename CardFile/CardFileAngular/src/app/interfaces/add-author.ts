export interface AddAuthor {
    name: string,
    surname: string
}
