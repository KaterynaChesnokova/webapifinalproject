export interface GetGenre {
    id: number,
    name: string,
    textsIds: number[]
}
