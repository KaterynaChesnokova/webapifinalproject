export interface LoggedUser {
    id:string,
    userName :string,
    token :string,
    expiration:Date,
    roles:string[]    
}
