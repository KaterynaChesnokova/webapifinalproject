export interface Register {
    email: string,
    password: string,
    userName: string,
    confirmPassword: string
}
