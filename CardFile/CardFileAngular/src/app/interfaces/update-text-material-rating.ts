export interface UpdateTextMaterialRating {
    id: number,
    rating: number
}
