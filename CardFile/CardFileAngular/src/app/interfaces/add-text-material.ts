export interface AddTextMaterial {
        title: string,
        body: string,
        authorsIds: number[],
        genresIds: number[]
}
