import { Type } from '@angular/core'

export interface ErrorResponse {
    Type:string,
    Message:string
}
