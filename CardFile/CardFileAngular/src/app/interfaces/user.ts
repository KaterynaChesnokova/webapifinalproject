export interface User {
    Id :string,
    Email :string,
    SecurityStamp:string,
    UserName :string,
    PasswordHash :string,
    PhoneNumber :string,
    NormalizedEmail :string,
    NormalizedUserName:string,
    TextsIds:number[]
}
