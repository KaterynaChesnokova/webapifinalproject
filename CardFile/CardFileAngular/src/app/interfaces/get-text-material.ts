export interface GetTextMaterial {
    id: number,
    title: string,
    body: string,
    publishDate: Date,
    rating: number,
    ratingGradeCount: number,
    publisherId: string,
    authorsIds: number[],
    genresIds:number[]
}
