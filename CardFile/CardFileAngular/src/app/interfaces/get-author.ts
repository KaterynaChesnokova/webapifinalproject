export interface GetAuthor {
    id: number,
    name: string,
    surname: string,
    textsIds: number[]
}
