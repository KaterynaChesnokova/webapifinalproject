export interface UpdateTextMaterial {
    id: number,
    title: string,
    body: string,
    authorsIds: number[],
    genresIds: number[]
}
