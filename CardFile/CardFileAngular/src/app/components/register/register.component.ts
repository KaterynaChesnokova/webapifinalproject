import { ErrorResponse } from './../../interfaces/error-response';
import { Router } from '@angular/router';
import { AuthService } from './../../services/auth-service/auth.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Register } from 'src/app/interfaces/register';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm!: FormGroup;
  registerError!: string;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.registerForm =  new FormGroup({
      email: new FormControl(''),
      userName: new FormControl(''),
      password: new FormControl(''),
      confirmPassword: new FormControl('')
    });
    this.registerError = '';
  }

 public register(){
    let userRegister:Register = this.registerForm.value;

    this.authService.register(userRegister).subscribe(
      ()=>{
        this.router.navigate(['/textmaterials']);
      },
      (exc) =>{
        this.registerError = exc.error;
      }
    )
  }
}
