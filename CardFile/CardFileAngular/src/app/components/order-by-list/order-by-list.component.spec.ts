import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderByListComponent } from './order-by-list.component';

describe('OrderByListComponent', () => {
  let component: OrderByListComponent;
  let fixture: ComponentFixture<OrderByListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderByListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderByListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
