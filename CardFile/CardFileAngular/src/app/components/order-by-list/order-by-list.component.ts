import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-order-by-list',
  templateUrl: './order-by-list.component.html',
  styleUrls: ['./order-by-list.component.css']
})
export class OrderByListComponent implements OnInit {

  orderBySelected:string='title';
  orderBy:string='title';
  order:string='asceding';

  @Output() onChanged=new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  onOrderChange(){
    if(this.orderBySelected=='title'){
       this.orderBy=this.orderBySelected;
       this.order='ascending';
    }
    else if(this.orderBySelected=='ratingAsc'){
      this.orderBy= 'rating';
      this.order='ascending';
    }
    else{
      this.orderBy = 'rating';
      this.order = 'descending';
    }
    this.onChanged.emit( [this.orderBy, this.order]);
  }
}
