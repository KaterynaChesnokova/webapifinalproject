import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TextMaterialWithGenreComponent } from './text-material-with-genre.component';

describe('TextMaterialWithGenreComponent', () => {
  let component: TextMaterialWithGenreComponent;
  let fixture: ComponentFixture<TextMaterialWithGenreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TextMaterialWithGenreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TextMaterialWithGenreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
