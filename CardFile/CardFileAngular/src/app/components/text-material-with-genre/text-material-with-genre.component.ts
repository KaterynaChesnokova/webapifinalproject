import { GetTextMaterial } from './../../interfaces/get-text-material';
import { Component, OnInit } from '@angular/core';
import { TextMaterialService } from 'src/app/services/text-material-service/text-material.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-text-material-with-genre',
  templateUrl: './text-material-with-genre.component.html',
  styleUrls: ['./text-material-with-genre.component.css']
})
export class TextMaterialWithGenreComponent implements OnInit {

  orderBy:string='title';
  order:string='asceding';

  textsWithGenre:GetTextMaterial[]=[];
  noTexts!:string;

  constructor(
    private textService:TextMaterialService,
    private route:ActivatedRoute,
    private router:Router
    ) { }

  ngOnInit(): void {
    let genre=this.route.snapshot.paramMap.get('genre');
    if(genre==null)
      this.router.navigate(['/textmaterials']);
    else{
      let texts = this.textService.getAllTextMatWithGenre(genre).subscribe((data)=>{
        if(data==null)
            this.noTexts=`No texts with genre ${genre} were found.`;
        else
          this.textsWithGenre=data;
      });
    }
  }

  onStarClick(){
    let genre=this.route.snapshot.paramMap.get('genre');
    this.textService.getAllTextMatWithGenre(<string>genre).subscribe((t) => {
            this.textsWithGenre = t;
      });
    }

  onChanged(item:any){
      this.orderBy=item[0];
      this.order=item[1];
  }
}
