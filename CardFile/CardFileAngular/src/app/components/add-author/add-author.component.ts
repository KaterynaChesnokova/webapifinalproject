import { AuthorService } from './../../services/author-service/author.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AddAuthor } from 'src/app/interfaces/add-author';

@Component({
  selector: 'app-add-author',
  templateUrl: './add-author.component.html',
  styleUrls: ['./add-author.component.css']
})

export class AddAuthorComponent implements OnInit {

  resultStr:string='';
  form!:FormGroup;
  error!:string;

  constructor(private authorService:AuthorService) { }

  ngOnInit(): void {
    this.error='';
    this.form=new FormGroup({
      name:new FormControl(''),
      surname:new FormControl('')
    });
  }

  addAuthor(){
    let newAuthor:AddAuthor=this.form.value;
    this.authorService.addAuthor(newAuthor).subscribe((data) => {
      this.resultStr=`Author ${newAuthor.name} ${newAuthor.surname} added!`;
    },
     (error) => {console.log(error)}
    );
  }
}
