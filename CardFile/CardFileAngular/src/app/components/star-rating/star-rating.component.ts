import { TextMaterialService } from 'src/app/services/text-material-service/text-material.service';
import { UpdateTextMaterialRating } from './../../interfaces/update-text-material-rating';
import { GetTextMaterial } from './../../interfaces/get-text-material';
import { Component, OnInit, Input, Output, ViewEncapsulation, EventEmitter } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.css'],
  encapsulation: ViewEncapsulation.Emulated
})
export class StarRatingComponent implements OnInit {

  @Input('chosen') chosen!:GetTextMaterial;
  private snackBarDuration: number = 2000;
  @Input('ratingArr') ratingArr = [];
  @Input('rating') private rating!: number;
  @Input('starCount') private starCount: number = 10;
  @Input('color') private color: string= 'red';
  @Output() private ratingUpdated = new EventEmitter();

  constructor(private snackBar: MatSnackBar,private textService: TextMaterialService) { }

  ngOnInit() {
    for (let index = 0; index < this.starCount; index++) {
      this.ratingArr.push(<never>index);
    }
  }
  onClick(rating:number) {
    debugger;
    let text= this.chosen;
    this.snackBar.open('You rated ' + rating + ' / ' + this.starCount, '', {
          duration: this.snackBarDuration
        });
        let newRating = {id:text.id,rating:rating} as UpdateTextMaterialRating;
        this.textService.updateTextMaterialRating(newRating).subscribe(()=>{
          })
        this.ratingUpdated.emit(rating);
        return false;
  }

  showIcon(index:number) {
    if (this.rating >= index + 1) {
      return 'star';
    } else {
      return 'star_border';
    }
  }
}
export enum StarRatingColor {
  primary = "primary",
  accent = "accent",
  warn = "warn"
}
