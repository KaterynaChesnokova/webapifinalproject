import { AddTextMaterial } from './../../interfaces/add-text-material';
import { Component, OnInit } from '@angular/core';
import { GenreService } from './../../services/genre-service/genre.service';
import { AuthorService } from './../../services/author-service/author.service';
import { GetAuthor } from './../../interfaces/get-author';
import { GetGenre } from './../../interfaces/get-genre';
import { FormGroup, FormControl, FormArray, FormBuilder } from '@angular/forms';
import { TextMaterialService } from './../../services/text-material-service/text-material.service';
import * as _ from "lodash"

@Component({
  selector: 'app-add-text-material',
  templateUrl: './add-text-material.component.html',
  styleUrls: ['./add-text-material.component.css']
})
export class AddTextMaterialComponent implements OnInit {

  resultStr:string='';
  form1!:FormGroup;

  form2!:FormGroup;
  addError!:string;

  authorOptions:GetAuthor[]=[];
  genreOptions:GetGenre[]=[];

    authItems!: FormArray;
    genreItems!: FormArray;

  constructor(
    private textService:TextMaterialService,
    private authorSerice:AuthorService,
    private genreService:GenreService,
    private formBuilder: FormBuilder
  ) {
    this.authorSerice.getAllAuthors().subscribe(
      (data)=>{
        this.authorOptions=data;
    });
    this.genreService.getAllGenres().subscribe(
      (data)=>{
      this.genreOptions=data;
      this.addCheckboxes();
    });
   }

  ngOnInit(): void {
    this.form1=new FormGroup({
        title:new FormControl(''),
        body:new FormControl(''),
      }); 
      this.form2 = this.formBuilder.group({
        authors: new FormArray([]),
        genres: new FormArray([])
      });
    }

  private addCheckboxes() {
    this.authItems=this.form2.get('authors') as FormArray;
    this.genreItems=this.form2.get('genres') as FormArray;

    this.authorOptions.forEach(() => this.authItems.push(new FormControl(false)));
    this.genreOptions.forEach(() => this.genreItems.push(new FormControl(false)));
  }

  addText():void{
    const temp1 = this.form2.value.authors
      .map((checked:any, i:number) => checked ? this.authorOptions[i].id : null) as number[];
    let selectedAuthors = temp1.filter(v => v !== null);

    const temp2 = this.form2.value.genres
    .map((checked:any, i:number) => checked ? this.genreOptions[i].id : null) as number[];
    let selectedGenres = temp2.filter(v => v !== null);

    let newText:AddTextMaterial=this.form1.value;
    newText.authorsIds=selectedAuthors;
    newText.genresIds=selectedGenres;

    this.textService.addTextMaterial(newText).subscribe((data :any) => {
      this.resultStr=`Text "${newText.title}" added!`;
    },
     (error) => {console.log(error)}
    );
  }
}
