import { GetAuthor } from './../../interfaces/get-author';
import { GetTextMaterial } from './../../interfaces/get-text-material';
import { Component, OnInit } from '@angular/core';
import { TextMaterialService } from 'src/app/services/text-material-service/text-material.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-text-material-with-author',
  templateUrl: './text-material-with-author.component.html',
  styleUrls: ['./text-material-with-author.component.css']
})
export class TextMaterialWithAuthorComponent implements OnInit {

  orderBy:string='title';
  order:string='asceding';

  textsWithAuthor:GetTextMaterial[]=[];
  noTexts!:string;

  constructor(
    private textService:TextMaterialService,
    private route:ActivatedRoute,
    private router:Router
    ) { }

  ngOnInit(): void {
    let authorName=this.route.snapshot.paramMap.get('name');
    let authorSurname=this.route.snapshot.paramMap.get('surname');

    if(authorName==null || authorSurname==null)
      this.router.navigate(['/textmaterials']);
    else{
      let texts = this.textService.getAllTextMatWithAuthor(authorName,authorSurname).subscribe((data)=>{
        if(data==null)
            this.noTexts=`No texts with author ${authorName} ${authorSurname} were found.`;
        else
          this.textsWithAuthor=data;
      });
    }
  }

  onStarClick(){
    let authorName=this.route.snapshot.paramMap.get('name');
    let authorSurname=this.route.snapshot.paramMap.get('surname');
    this.textService.getAllTextMatWithAuthor(<string>authorName,<string>authorSurname).subscribe((t) => {
            this.textsWithAuthor = t;
          });
    }

    onChanged(item:any){
      this.orderBy=item[0];
      this.order=item[1];
  }
}
