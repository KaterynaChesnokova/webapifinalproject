import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TextMaterialWithAuthorComponent } from './text-material-with-author.component';

describe('TextMaterialWithAuthorComponent', () => {
  let component: TextMaterialWithAuthorComponent;
  let fixture: ComponentFixture<TextMaterialWithAuthorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TextMaterialWithAuthorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TextMaterialWithAuthorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
