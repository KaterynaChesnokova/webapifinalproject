import { AddGenre } from './../../interfaces/add-genre';
import { GenreService } from './../../services/genre-service/genre.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-genre',
  templateUrl: './add-genre.component.html',
  styleUrls: ['./add-genre.component.css']
})
export class AddGenreComponent implements OnInit {

  resultStr:string='';
  form!:FormGroup;
  error!:string;

  constructor(private genreService:GenreService) { }

  ngOnInit(): void {
    this.error='';
    this.form=new FormGroup({
      name:new FormControl('')
    });
  }

  public addGenre(){
    let newGenre:AddGenre=this.form.value;
    this.genreService.addGenre(newGenre).subscribe(() => {
      this.resultStr=`Genre ${newGenre.name} added!`;
    },
     (error) => {console.log(error)}
    );
  }
}
