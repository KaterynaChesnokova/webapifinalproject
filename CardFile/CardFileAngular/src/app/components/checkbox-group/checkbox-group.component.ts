import { CheckBoxItem } from './../../interfaces/check-box-item';
import { Component, OnInit,Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-checkbox-group',
  templateUrl: './checkbox-group.component.html',
  styleUrls: ['./checkbox-group.component.css']
})
export class CheckboxGroupComponent implements OnInit {

  @Input() options = Array<CheckBoxItem>();
  @Output() toggle = new EventEmitter<any[]>();

  constructor() { }

  ngOnInit(): void {
  }

  onToggle() {
    const checkedOptions = this.options.filter(x => x.checked);
    // this.selectedValues = checkedOptions.map(x => x.value);
    this.toggle.emit(checkedOptions.map(x => x.value));
   }

  //  onAuthorsChange(value) {
  //   this.userModel.roles = value;
  //   console.log('Checked:' , this.userModel.roles);
  //  }
  
}
