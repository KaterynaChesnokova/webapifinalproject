import { AuthService } from './../../services/auth-service/auth.service';
import { Router } from '@angular/router';
import { AddAuthor } from './../../interfaces/add-author';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthorService } from './../../services/author-service/author.service';
import { GetAuthor } from './../../interfaces/get-author';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {

  roles:string[]=[];

  authors!:GetAuthor[];
  form!:FormGroup;
  error!:string;

  constructor(
    private authService: AuthService,
    private authorService:AuthorService,
    private router:Router
    ) { }

  ngOnInit(): void {
    let currentUser=this.authService.getCurrentUserValue();
    if(currentUser!=null)
      this.roles=currentUser.roles;
    this.error='';
    this.authorService.getAllAuthors().subscribe((data)=>{
      this.authors=data;
    });
    this.form=new FormGroup({
      name:new FormControl(''),
      surname:new FormControl('')
    });
  }

  onSelect(author:GetAuthor){
    this.router.navigate(['/textmaterials/withauthor', author.name, author.surname]);
  }

  onDelete(author:GetAuthor){
    if(confirm(`Are you sure you want to delete ${author.name} ${author.surname}?`)) {
    this.authorService.deleteAuthor(author.id).subscribe((x)=>{
      this.authorService.getAllAuthors().subscribe((data)=>{
        this.authors=data;
      });
    })
    
  }
  else{
    this.authorService.getAllAuthors().subscribe((data)=>{
      this.authors=data;
    });
    }
}
}
