import { AuthService } from 'src/app/services/auth-service/auth.service';
import { TextMaterialService } from './../../services/text-material-service/text-material.service';
import { GetTextMaterial } from './../../interfaces/get-text-material';
import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import * as _ from "lodash"
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-text-materials',
  templateUrl: './text-materials.component.html',
  styleUrls: ['./text-materials.component.css'],
  encapsulation: ViewEncapsulation.Emulated
})
export class TextMaterialsComponent implements OnInit {

  orderBy:string='title';
  order:string='asceding';

  texts:GetTextMaterial[]=[];
  roles:string[]=[];
  
  constructor(
    private textService:TextMaterialService,
    private authService:AuthService,
    private snackBar: MatSnackBar
    ) {
      this.textService.getAllTextMaterials().subscribe((data)=>{
      this.texts=data;
    });
    }

     ngOnInit(){
       let currentUser=this.authService.getCurrentUserValue();
      if(currentUser!=null)
        this.roles=currentUser.roles;
     }

     onStarClick(){
      this.textService.getAllTextMaterials().subscribe((t) => {
              this.texts = t;
              debugger;
            });
          }

    onChanged(item:any){
      this.orderBy=item[0];
      this.order=item[1];
    }

    onDelete(text:GetTextMaterial){
      if(confirm(`Are you sure you want to delete ${text.title}`)) {
      this.textService.deleteTextMaterial(text.id).subscribe((x)=>{
        this.textService.getAllTextMaterials().subscribe((data)=>{
          this.texts=data;
        });
      })
    }
  }
}