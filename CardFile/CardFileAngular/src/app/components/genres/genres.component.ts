import { AuthService } from 'src/app/services/auth-service/auth.service';
import { Router } from '@angular/router';
import { AddGenre } from './../../interfaces/add-genre';
import { GenreService } from './../../services/genre-service/genre.service';
import { FormGroup, FormControl } from '@angular/forms';
import { GetGenre } from './../../interfaces/get-genre';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-genres',
  templateUrl: './genres.component.html',
  styleUrls: ['./genres.component.css']
})
export class GenresComponent implements OnInit {

  roles:string[]=[];

  genres!:GetGenre[];
  form!:FormGroup;
  error!:string;

  constructor(
    private authService: AuthService,
    private genreService:GenreService,
    private router:Router
    ) { }

  ngOnInit(): void {
    let currentUser=this.authService.getCurrentUserValue();
    if(currentUser!=null)
      this.roles=currentUser.roles;
    this.error='';
    this.genreService.getAllGenres().subscribe((data)=>{
      this.genres=data;
    });
    this.form=new FormGroup({
      name:new FormControl('')
    });
  }

  onSelect(genre:GetGenre){
    this.router.navigate(['/textmaterials/withgenre',genre.name]);
  }

  onDelete(genre:GetGenre){
    if(confirm(`Are you sure you want to delete ${genre.name}`)) {
    this.genreService.deleteGenre(genre.id).subscribe((x)=>{
      this.genreService.getAllGenres().subscribe((data)=>{
        this.genres=data;
      });
    })
  }
}
}
