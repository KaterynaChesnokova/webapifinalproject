import { AuthService } from './../services/auth-service/auth.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';

@Injectable({providedIn: 'root'})
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router){
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
    let currentUser  = this.authService.getCurrentUserValue();
    if(currentUser!=null)
    {
        let roles:string[]=currentUser.roles;
        return roles.some(x => currentUser?.roles.includes("Admin"));
    }

    this.router.navigate(['/login']);
    return false;
  }
}